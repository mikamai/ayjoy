namespace :drupal do
  namespace :configure do
    task :stage do
      sudo "cp #{latest_release}/sites/default/settings.#{stage_name}.php #{latest_release}/sites/default/settings.php"
    end
  end
end