before 'deploy:restart', 'mikamai:permissions:fix', 'mikamai:production:symlink'
before 'deploy:start', 'mikamai:permissions:fix', 'mikamai:production:symlink'

namespace :mikamai do
  
  namespace :production do
    task :symlink, :except => { :no_release => true } do
      sudo "rm -rf /var/www/#{application}"
      sudo "ln -s #{latest_release} /var/www/#{application}"
    end
  end
  
  namespace :permissions do
    task :fix, :except => { :no_release => true } do
      sudo "chown -R www-data:www-data #{latest_release}"
    end
  end
  
end
