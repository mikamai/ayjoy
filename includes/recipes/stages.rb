desc "deploy to development environment"
task :development do
  set :stage_name, "development"
  set :application, "ayjoy.mikamai.com"

  role :web, "danton.mikamai.com:8888", :primary => true
  role :db, "danton.mikamai.com:8888", :primary => true
  set :user, "drupal"
  set :password, "sperC0l4t0"
  set :remote_mysqldump, "/usr/bin/mysqldump"
  set :deploy_to, "/var/apps/#{application}"

  set :db_user, "moretto"
  set :db_password, "sironi11"
  set :db_name, "ayjoy"
end

desc "deploy to staging environment"
task :staging do 
  set :stage_name, "staging"
end

desc "deploy to production"
task :production do
  set :stage_name, "production"
  set :application, "ayjoy.it"

  role :web, "ayjoy.it", :primary => true
  role :db, "ayjoy.it", :primary => true
  set :user, "drupal"
  set :password, "presePPE3aK"
  set :remote_mysqldump, "/usr/bin/mysqldump"
  set :deploy_to, "/var/apps/#{application}"

  set :db_user, "moretto"
  set :db_password, "sironi11"
  set :db_name, "ayjoy"
end
