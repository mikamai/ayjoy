
<div id="products-single"> <!-- Primo div che contiene tutti gli item prodotto -->
  <div class="product-item">
	  <h3> <?php print $node->title ?> </h3>
        <div class="pruduct-item-image">
         <?php foreach ($node->field_image_cache as $image) :  ?>
         	<?php if ($image['filename']) $images[] = $image;   ?>
         <?php endforeach; ?>
         
    	  <?php print theme('uc_product_image', $images);  ?>
   	   </div>
<? 		
$disp = false;
/* temporary fix to hide empty fields:
* only display the field if any of the items don't match the empty value conditions */
foreach ($items as $item) {
if(!empty($item['view']) && $item['view'] != "<br />\n<br class=\"clear\" />") {
$disp = true;
}
} ?>
      <p class="descrizione"> <?php print $node->content['body']['#value'] ?>  </p>
  
      <div class="field-label"><?php print t('Modalità d\'uso:') ?> </div><p><?php print $node->field_modalit_duso[0]['view'] ?></p>
      <div class="field-label"><?php print t('Ingredienti:') ?> </div><p><?php print $node->field_ingredienti[0]['view'] ?></p>
      <div class="field-label"><?php print t('Avvertenze:') ?> </div><p><?php print $node->field_avvertenze[0]['view'] ?></p>




         
      <br class="clear">
    <div>
    
    <!-- Ora la barra con i dati carrello -->
    <div id="product-item-cart-down">
      <div id="product-item-cart-up">
          <div class="cart-image"> <?php print theme('image',path_to_theme().'/css/images/cart-2.png') ?></div>      
          <p class="price"><span class="price"><?php print uc_currency_format($node->sell_price) ?></span></p>
          <div class="buy-link">
            <?php  
            	print theme('uc_product_add_to_cart_blocco_catalogo', $node); 
            ?>
            
          </div>
          <br class="clear" />
      </div>
    </div>
    
    
  </div>
</div>
</div>