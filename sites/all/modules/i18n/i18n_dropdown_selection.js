if (Drupal.jsEnabled) {
  $(document).ready(function(){
    $('#edit-select').hide();
    $('#edit-lang-path').change(function(){
      var i18n_path = '';
      i18n_path = $('#edit-lang-path option:selected').val();
      window.location.href = "/" + i18n_path;
      });
    });
}
