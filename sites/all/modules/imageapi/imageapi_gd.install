<?php
// $Id: imageapi_gd.install,v 1.3.2.2 2008/05/29 02:47:28 drewish Exp $

function imageapi_gd_requirements($phase) {
  $requirements = array();
  $t = get_t();
 
  $gd = function_exists('imagegd2');

  if (!$gd) {
    $requirements['imageapi_gd'] = array(
      'title' =>  $t('GD library'),
      'value' => $t('Not installed'),
      'severity' => REQUIREMENT_ERROR,
      'description' => $t('The GD library for PHP is missing or outdated. Please check the <a href="@url">PHP image documentation</a> for information on how to correct this.', array('@url' => 'http://www.php.net/manual/en/image.setup.php')),
    );
    return $requirements;
  } 

  // Check image format support 
  foreach (array('gif', 'jpeg', 'png') as $format) {
    if (function_exists('imagecreatefrom'. $format)) continue;
    $requirements['imageapi_gd_'. $format] = array(
      'title' => $t('GD !format Support', array('!format' => drupal_ucfirst($format))),
      'value' => $t('Not installed'),
      'severity' => REQUIREMENT_ERROR,
      'description' => $t('PHP GD was not compiled with %format support.', array('%format' => $format)), 
    );
  }


  // check non required stuff aka not installation blockers.
  if ($phase == 'runtime') {
    if (!function_exists('imagerotate')) {
      require_once drupal_get_path('module', 'imageapi') .'/imagerotate.inc';
    }
    if (IMAGEAPI_IMAGEROTATE_PHP == 1) {
      $requirements['imageapi_gd_imagerotate'] = array(
        'title' => $t('GD Image Rotation'),
        'value' => $t('Low Quality / Poor Performance'),
        'severity' => REQUIREMENT_WARNING,
        'description' => $t('The installed version of PHP GD does not support image rotations. It was probably compiled using the official GD libraries from http://www.libgd.org instead of the GD library bundled with PHP. You should recompile PHP --with-gd using the bundled GD library. See: @url. An implementation of imagerotate in PHP will used in the interim.',  array('@url' => 'http://www.php.net/manual/en/image.setup.php')),
      );
    }

    if (!function_exists('imagefilter')) {
      require_once drupal_get_path('module', 'imageapi') .'/imagefilter.inc';
    }
    if (IMAGEAPI_IMAGEFILTER_PHP == 1) {
      $requirements['imageapi_gd_imagefilter'] = array(
        'title' => $t('GD Image Filtering'),
        'value' => $t('Low Quality / Poor Performance'),
        'severity' => REQUIREMENT_WARNING,
        'description' => $t('The installed version of PHP GD does not support image filtering(desaturate, blur, negate, etc). It was probably compiled using the official GD libraries from http://www.libgd.org instead of the GD library bundled with PHP. You should recompile PHP --with-gd using the bundled GD library. See @url. An implementation of imagefilter in PHP will be used in the interim.',  array('@url' => 'http://www.php.net/manual/en/image.setup.php')),
      );
    }
  }
  return $requirements;
}
