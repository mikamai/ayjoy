Imagefield Gallery is specifically designed to make theming and displaying
imagefield images easy. Imagefield Gallery makes use of nodeapi and
other non-obtrusive Drupal technologies, so installing it and using it are
as simple as possible, and have very little impact on a site.  If you'd
like to try it out, uninstalling it will make it go back to the
way your site was before hand.

Imagefield Gallery is currently made up of two separate
modules.  The first is the imagefield_gallery module which does nothing on
its own.  The second is the imagefield_gallery_lightbox2 module.  This
module allows you to create galleries of images on a node that has an
imagefield on it.  Imagefield_gallery is designed to work with nodes that
have only one imagefield, it does not currently work with nodes that have
more than one imagefield.  Results cannot be garunteed in these
situations.  For best results set your imagefield to be a multi-value
imagefield and then use the links below to administrate the look of your
gallery.  Imagefield_gallery works on a node by node basis, so you can set
completely different css settings on nodes, even if they share the same
gallery type, and same imagefield!
