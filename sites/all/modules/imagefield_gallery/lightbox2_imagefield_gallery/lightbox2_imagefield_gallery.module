<?php
//$Id: lightbox2_imagefield_gallery.module,v 1.1 2008/09/17 16:10:30 eclipsegc Exp $

/**
 * implementation of hook_enable()
 */

function lightbox2_imagefield_gallery_enable() {
  lightbox2_imagefield_gallery_settings();
}

/**
 * implementation of imagefield_gallery_hook_settings()
 * function checks to see if the proper key existings
 * in the imagefield_gallery_galleries variable, and
 * if it does, does nothing, and if it doesn't it adds
 * the proper settings to the variable.
 */

function lightbox2_imagefield_gallery_settings() {
  $settings = variable_get('imagefield_gallery_galleries', array());
  if (!array_key_exists('lightbox2', $settings)) {
    $settings['lightbox2'] = t('Lightbox2 Gallery');
    variable_set('imagefield_gallery_galleries', $settings);
  }
}

/**
 * imagefield_gallery_hook_details is meant to keep track of
 * all of our stylistic variables for a given gallery type
 * these variables then need to be rendered in an acceptable
 * fashion for display on the content types admin page
 */

function lightbox2_imagefield_gallery_details($type, $field) {
  $output = '';
  // we're going to create an array of all our gallery's style related variables.
  $styles = variable_get('lightbox2_imagefield_gallery_css', array());
  // once set we will use this function to manually parse it into a presentation we like for the content types admin page
  $headers = array(t('Attribute'), t('Value'));
  $rows = array();
  $rows[] = array(t('Thumbnail Width'), $styles[$type][$field]['width'] .'px');
  $rows[] = array(t('Thumbnail Height'), $styles[$type][$field]['height'] .'px');
  $rows[] = array(t('Thumbnail Top Margin'), $styles[$type][$field]['margins']['top'] .'px');
  $rows[] = array(t('Thumbnail Right Margin'), $styles[$type][$field]['margins']['right'] .'px');
  $rows[] = array(t('Thumbnail Bottom Margin'), $styles[$type][$field]['margins']['bottom'] .'px');
  $rows[] = array(t('Thumbnail Left Margin'), $styles[$type][$field]['margins']['left'] .'px');
  $rows[] = array(t('Thumbnail Borders'), $styles[$type][$field]['border']['border_width'] .' '. $styles[$type][$field]['border']['border_style'] .' '. $styles[$type][$field]['border']['color_style_textfield']);
  
  $output .= theme('table', $headers, $rows);
  return $output;
}

/**
 * implementation of hook_imagefield_gallery_content_types()
 */

function lightbox2_imagefield_gallery_content_types($form = array(), $var = array(), $type, $field, $cache = array()) {
  drupal_add_css(drupal_get_path('module', 'imagefield_gallery_lightbox2') .'/css/content_types.css');

  $form[$type][$field]['gallery'] = array(
    '#type' => 'select',
    '#title' => t('@type @field field gallery type', array('@type' => $type, '@field' => $field)),
    '#default_value' => isset($var[$type][$field]['gallery']) ? $var[$type][$field]['gallery'] : 'none',
    '#options' => array(
      imagefield_gallery_get_gallery_types(),
    ),
    '#tree' => TRUE,
  );
  $form[$type][$field]['images'] = array(
    '#type' => 'fieldset',
    '#title' => t('@type @field field image settings', array('@type' => $type, '@field' => $field)),
    '#collapsible' => FALSE,
    '#description' => t('Select the imagecache settings you would like to use (if any) for the @type content type @field field.', array('@type' => $type, '@field' => $field)),
    '#tree' => TRUE,
  );
  $form[$type][$field]['images']['thumbnail'] = array(
    '#type' => 'select',
    '#title' => t('@type @field field thumbnail', array('@type' => $type, '@field' => $field)),
    '#default_value' => isset($var[$type][$field]['images']['thumbnail']) ? $var[$type][$field]['images']['thumbnail'] : 'none',
    '#options' => $cache,
    '#tree' => TRUE,
  );
  $form[$type][$field]['images']['preview'] = array(
    '#type' => 'select',
    '#title' => t('@type @field field preview', array('@type' => $type, '@field' => $field)),
    '#default_value' => isset($var[$type][$field]['images']['preview']) ? $var[$type][$field]['images']['preview'] : 'none',
    '#options' => $cache,
    '#tree' => TRUE,
  );
  return $form;
}

/**
 * implementation of hook_imagefield_gallery_content_types_validate()
 */

function lightbox2_imagefield_gallery_content_types_validate($form_id, $form_values, $form) {
  $types = imagefield_gallery_nodetypes();
  foreach ($types as $type) {
    if (is_array($form_values[$type['type']][$type['name']]) && $form_values[$type['type']][$type['name']]['gallery'] != 'none') {
      if ($form_values[$type['type']][$type['name']]['images']['thumbnail'] == 'none' || $form_values[$type['type']][$type['name']]['images']['preview'] == 'none') {
        form_set_error('', t('You must select an imagecache setting for the @type content type $field field!', array('@type' => $type['type'], '@field' => $type['name'])));
      }
    }
  }
}

/**
 * implementation of hook_imagefield_gallery_admin_validate()
 */

function lightbox2_imagefield_gallery_admin_validate($form_id, $form_values) {
  $types = imagefield_gallery_nodetypes();
  foreach ($types as $type) {
    if (!empty($form_values['lightbox2'][$type['type']][$type['name']]['width'])) {
      if (!is_numeric($form_values['lightbox2'][$type['type']][$type['name']]['width'])) {
        form_set_error('', t('The @type @field width value must be a number!', array('@type' => $type['type'], '@field' => $type['name'])));
      }
    }
    if (!empty($form_values['lightbox2'][$type['type']][$type['name']]['height'])) {
      if (!is_numeric($form_values['lightbox2'][$type['type']][$type['name']]['height'])) {
        form_set_error('', t('The @type @field height value must be a number!', array('@type' => $type['type'], '@field' => $type['name'])));
      }
    }
    if (!empty($form_values['lightbox2'][$type['type']][$type['name']]['margins']['top'])) {
      if (!is_numeric($form_values['lightbox2'][$type['type']][$type['name']]['margins']['top'])) {
        form_set_error('', t('The @type @field top margin value must be a number!', array('@type' => $type['type'], '@field' => $type['name'])));
      }
    }
    if (!empty($form_values['lightbox2'][$type['type']][$type['name']]['margins']['right'])) {
      if (!is_numeric($form_values['lightbox2'][$type['type']][$type['name']]['margins']['right'])) {
        form_set_error('', t('The @type @field right margin value must be a number!', array('@type' => $type['type'], '@field' => $type['name'])));
      }
    }
    if (!empty($form_values['lightbox2'][$type['type']][$type['name']]['margins']['bottom'])) {
      if (!is_numeric($form_values['lightbox2'][$type['type']][$type['name']]['margins']['bottom'])) {
        form_set_error('', t('The @type @field bottom margin value must be a number!', array('@type' => $type['type'], '@field' => $type['name'])));
      }
    }
    if (!empty($form_values['lightbox2'][$type['type']][$type['name']]['margins']['left'])) {
      if (!is_numeric($form_values['lightbox2'][$type['type']][$type['name']]['margins']['left'])) {
        form_set_error('', t('The @type @field left margin value must be a number!', array('@type' => $type['type'], '@field' => $type['name'])));
      }
    }
    if (!empty($form_values['lightbox2'][$type['type']][$type['name']]['border']['color_style_textfield'])) {
      if (!preg_match('/^#[0-9A-Fa-f]{6}$/', $form_values['lightbox2'][$type['type']][$type['name']]['border']['color_style_textfield'])) {
        form_set_error('', t('The @type @field border color value must be a hexidecimal number!', array('@type' => $type['type'], '@field' => $type['name'])));
      }
    }
  }
}

/**
 * implementation of hook_imagefield_gallery_admin()
 * @param $type
 *   the node type so that the validate and submit functions
 *   can operate more easily.  '#tree' => TRUE is highly
 *   recommended in this case to eliminate potential
 *   conflicts in naming convention.
 *
 * this entire form function will be used to help create
 * the css for this gallery type.  the imagefield_gallery_lightbox2_css
 * variable stores all the css for this gallery type by node type.
 * this function is a fairly good example of what you should be
 * striving for in creating your own admin pages.
 *
 * also, you'll noted that the primary form elements are all contained
 * within $form['lightbox2'].  It is imperative that you also name a
 * containing form element with the same naming convention as your module.
 * 
 */

function lightbox2_imagefield_gallery_admin($type, $field) {
  drupal_add_css(drupal_get_path('module', 'lightbox2_imagefield_gallery') .'/css/admin.css');
  $var = variable_get('lightbox2_imagefield_gallery_css', array());
  $form['variable'] = array(
    '#type' => 'value',
    '#value' => 'lightbox2',
  );
  $form['lightbox2'] = array(
    '#type' => 'fieldset',
    '#title' => t('Lightbox2 @type @field field Attributes', array('@type' => $type, '@field' => $field)),
    '#tree' => TRUE,
    '#attributes' => array('class' => 'lightbox2_admin_fieldset'),
  );
  $form['lightbox2'][$type][$field] = array(
    '#type' => 'fieldset',
    '#title' => t('Thumbnail Attributes'),
    '#collapsible' => FALSE,
    '#description' => t('Set the width and height, in pixels, you desire your thumbnails to be.'),
    '#tree' => TRUE,
    '#attributes' => array('class' => 'lightbox2_thumbnail_fieldset'),
  );
  $form['lightbox2'][$type][$field]['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Thumbnail Width'),
    '#size' => 10,
    '#maxlength' => 4,
    '#default_value' => isset($var[$type][$field]['width']) ? $var[$type][$field]['width'] : 100,
    '#required' => TRUE,
    '#tree' => TRUE,
  );
  $form['lightbox2'][$type][$field]['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Thumbnail Height'),
    '#size' => 10,
    '#maxlength' => 4,
    '#default_value' => isset($var[$type][$field]['height']) ? $var[$type][$field]['height'] : 100,
    '#required' => TRUE,
    '#tree' => TRUE,
  );
  $form['lightbox2'][$type][$field]['margins'] = array(
    '#prefix' => '<div style="clear:both"></div>',
    '#type' => 'fieldset',
    '#title' => t('Margin Attributes'),
    '#tree' => TRUE,
    '#attributes' => array('class' => 'lightbox2_margin_fieldset'),
  );
  $form['lightbox2'][$type][$field]['margins']['top'] = array(
    '#type' => 'textfield',
    '#title' => t('Top Margin'),
    '#default_value' => isset($var[$type][$field]['margins']['top']) ? $var[$type][$field]['margins']['top'] : 10,
    '#size' => '5',
    '#maxlength' => 3,
    '#required' => TRUE,
    '#tree' => TRUE,
  );
  $form['lightbox2'][$type][$field]['margins']['right'] = array(
    '#type' => 'textfield',
    '#title' => t('Right Margin'),
    '#default_value' => isset($var[$type][$field]['margins']['right']) ? $var[$type][$field]['margins']['right'] : 10,
    '#size' => '5',
    '#maxlength' => 3,
    '#required' => TRUE,
    '#tree' => TRUE,
  );
  $form['lightbox2'][$type][$field]['margins']['bottom'] = array(
    '#type' => 'textfield',
    '#title' => t('Bottom Margin'),
    '#default_value' => isset($var[$type][$field]['margins']['bottom']) ? $var[$type][$field]['margins']['bottom'] : 10,
    '#size' => '5',
    '#maxlength' => 3,
    '#required' => TRUE,
    '#tree' => TRUE,
  );
  $form['lightbox2'][$type][$field]['margins']['left'] = array(
    '#type' => 'textfield',
    '#title' => t('Left Margin'),
    '#default_value' => isset($var[$type][$field]['margins']['left']) ? $var[$type][$field]['margins']['left'] : 10,
    '#size' => '5',
    '#maxlength' => 3,
    '#required' => TRUE,
    '#tree' => TRUE,
  );

  $form['lightbox2'][$type][$field]['border'] = array(
    '#type' => 'fieldset',
    '#title' => t('Border Attributes'),
    '#tree' => TRUE,
  );
  if (module_exists('colorpicker')) {
    $form['lightbox2'][$type][$field]['border']['color_style'] = array(
      '#type' => 'colorpicker',
      '#title' => t('Border Color Picker'),
    );

    $form['lightbox2'][$type][$field]['border']['color_style_textfield'] = array(
      '#type' => 'colorpicker_textfield',
      '#title' => t('Border Color'),
      '#default_value' => isset($var[$type][$field]['border']['color_style_textfield']) ? $var[$type][$field]['border']['color_style_textfield'] : '#ffffff',
      '#colorpicker' => 'color_style',
      '#tree' => TRUE,
    );
  }
  else {
    $form['lightbox2'][$type][$field]['border']['color_style_textfield'] = array(
      '#type' => 'textfield',
      '#title' => t('Border Color'),
      '#default_value' => isset($var[$type][$field]['border']['color_style_textfield']) ? $var[$type][$field]['border']['color_style_textfield'] : '#ffffff',
      '#size' => '8',
      '#maxlength' => 7,
      '#description' => t('Please insert a color in hexidecimal value.  Do NOT forget the #.'),
      '#required' => TRUE,
      '#tree' => TRUE,
    );
    drupal_set_message('Installation of the colorpicker module will make your overal experience much more pleasent.');
  }
  $form['lightbox2'][$type][$field]['border']['border_style'] = array(
    '#type' => 'select',
    '#title' => t('Border Style'),
    '#default_value' => isset($var[$type][$field]['border']['border_style']) ? $var[$type][$field]['border']['border_style'] : 'none',
    '#options' => array(
      'none' => t('No Border'),
      'hidden' => t('Hidden'),
      'dotted' => t('Dotted'),
      'dashed' => t('Dashed'),
      'solid' => t('Solid'),
      'double' => t('Double'),
      'groove' => t('Groove'),
      'ridge' => t('Ridge'),
      'inset' => t('Inset'),
      'outset' => t('Outset'),
    ),
    '#description' => t('Select the border style you would like.'),
    '#tree' => TRUE,
  );
  $form['lightbox2'][$type][$field]['border']['border_width'] = array(
    '#type' => 'select',
    '#title' => t('Border Width'),
    '#default_value' => isset($var[$type][$field]['border']['border_width']) ? $var[$type][$field]['border']['border_width'] : '1px',
    '#options' => array(
      '0' => t('No Border'),
      '1px' => t('1 Pixel'),
      '2px' => t('2 Pixels'),
      '3px' => t('3 Pixels'),
      '4px' => t('4 Pixels'),
      '5px' => t('5 Pixels'),
      '6px' => t('6 Pixels'),
      '7px' => t('7 Pixels'),
      '8px' => t('8 Pixels'),
      '9px' => t('9 Pixels'),
      '10px' => t('10 Pixels'),
    ),
    '#description' => t('The Number of pixels wide you would like your border to be.'),
    '#tree' => TRUE,
  );
  return $form;
}

/**
 * implementation of imagefield_gallery_hook_admin_submit()
 * because we keyed the form by node type, we can just set the
 * variable equal to $form_values['lightbox2']
 */

function lightbox2_imagefield_gallery_admin_submit($form_id, $form_values) {
  $var = variable_get('lightbox2_imagefield_gallery_css', array());
  foreach($form_values['lightbox2'] as $name => $type) {
    foreach ($type as $fname => $field) {
      $var[$name][$fname] = $field;
    }
  }
  variable_set('lightbox2_imagefield_gallery_css', $var);
}

/* ---------- Theme Functions ----------- */

/**
 * implementation of theme_hook_imagefield_gallery()
 * @param $images
 *   $images is an array of all the images in the imagefield
 *   associated with the node type.
 *
 * @param $node
 *   the entire node object is passed in for a number of reasons
 *   primarily this would allow a using with a specific need to
 *   still leverage this module to manage their gallery types
 *   even if they weren't using imagefield.  This isn't plausible
 *   with this version of the module, but should be more so soon
 *
 * the end intention is to unify the output of a number of different
 * modules so that imagefield isn't the only option.
 */

function theme_lightbox2_imagefield_gallery($images = array(), $node, $field) {
  $output = '';
  $count = count($images);
  $var = variable_get('imagefield_gallery', array());
  if ($count) {
    $i = 0;
    if (!module_exists('lightbox2')) {
      static $js_added = FALSE;
      if (!$js_added) {
        $js_added = TRUE;
        $js_settings = array(
          'use_alt_layout' =>  false,
          'disable_zoom' => false,
          'force_show_nav' => false,
          'group_images' => TRUE,
          'disable_for_gallery_lists' => TRUE,
          'node_link_text' => check_plain(t('View Image Details')),
          'image_count' => check_plain(t('Image !current of !total')),
          'lite_press_x_close' => t('press !x to close', array('!x' => '<a href="#" onclick="hideLightbox(); return false;"><kbd>x</kbd></a>')),
        );
        drupal_add_js(array('lightbox2' => $js_settings), 'setting');
        drupal_add_js(drupal_get_path('module', 'lightbox2_imagefield_gallery') .'/js/lightbox.js');
        drupal_add_css(drupal_get_path('module', 'lightbox2_imagefield_gallery') .'/css/lightbox.css');
      }
    }

    $output .= '<span class="'. $node->type .'_'. $field .'_gallery_text">'. t('Click the images below for larger versions:') .'</span>';
    $output .= '<div class="'. $node->type .'_'. $field .'_gallery_box">';
    $thumbnail = $var[$node->type][$field]['images']['thumbnail'];
    $preview = $var[$node->type][$field]['images']['preview'];
    while ($i < $count) {
      if ($images[$i]['filepath']) {
        $file['filepath'] = $images[$i]['filepath'];
        if ($preview == '_original') {
          $imagecache_path = base_path() . $file['filepath'];
        }
        else {
          $imagecache_path = base_path() . file_directory_path() .'/imagecache/'. $preview .'/'. $file['filepath'];
        }
        $output .= '<div class="'. $node->type .'_'. $field .'_image_field_thumbnail">';
        $output .= '<a href="'. $imagecache_path .'" rel="lightbox['. $node->nid .']" title="'. check_plain($images[$i]['title']) .'">';
        if ($thumbnail == '_original') {
          $output .= theme('image', $file['filepath'], $images[$i]['alt'], $images[$i]['title']);
        }
        else {
          $output .= theme('imagecache', $thumbnail, $file['filepath'], $images[$i]['alt'], $images[$i]['title']);
        }
        $output .= '</a>';
        $output .= '</div>';
      }
      $i++;
    }

  $output .= '<div class="clear_both"></div>';
  $output .= '</div>';
  }
  return $output;
}

/**
 * implementation of theme_hook_imagefield_gallery_css_render()
 * @param $type
 *   a node type for node-specific css via the !type variable
 *   in the t() function.
 *
 * the intent of this function is to set up the necessary
 * css that a gallery type will need, this may include a
 * number of other theme functions called by this function
 * as necessary.
 */

function theme_lightbox2_imagefield_gallery_css_render($type, $field) {
  $output = t('
/* ----- !type !field general ------------------------------------------------------------------- */


div.!type_!field_gallery_box {
  width:100%;
  clear:both;
  display:block;
}

/* ----- !type !field thumbnails ---------------------------------------------------------------- */

div.!type_!field_image_field_thumbnail {
  display: inline;
  float: left;
  overflow: hidden;
  !margins
  !borders
  !width
  !height
}

div.clear_both {
  clear: both;
}', array('!type' => $type, '!field' => $field, '!margins' => theme('lightbox2_imagefield_gallery_css_margins', $type, $field), '!borders' => theme('lightbox2_imagefield_gallery_css_borders', $type, $field), '!width' => theme('lightbox2_imagefield_gallery_css_width', $type, $field), '!height' => theme('lightbox2_imagefield_gallery_css_height', $type, $field)));
  return $output;
}

/**
 * helper function theme_lightbox2_imagefield_gallery_css_render()
 */

function theme_lightbox2_imagefield_gallery_css_margins($type, $field) {
  $var = variable_get('lightbox2_imagefield_gallery_css', array());
  if (isset($var[$type][$field])) {
    return '  margin:'. $var[$type][$field]['margins']['top'] .'px '. $var[$type][$field]['margins']['right'] .'px '. $var[$type][$field]['margins']['bottom'] .'px '.  $var[$type][$field]['margins']['left'] .'px;';
  }
  else {
    return '  margin:10px;';
  }
}

/**
 * helper function imagefield_gallery_lightbox2_css_render()
 */

function theme_lightbox2_imagefield_gallery_css_borders($type, $field) {
  $var = variable_get('lightbox2_imagefield_gallery_css', array());
  if (isset($var[$type][$field])) {
    return '  border:'. $var[$type][$field]['border']['border_width'] .' '. $var[$type][$field]['border']['border_style'] .' '. $var[$type][$field]['border']['color_style_textfield'] .';';
  }
  else {
    return '  border:1px solid #000000;';
  }
}

/**
 * helper function lightbox2_imagefield_gallery_css_render()
 */

function theme_lightbox2_imagefield_gallery_css_width($type, $field) {
  $var = variable_get('lightbox2_imagefield_gallery_css', array());
  if (isset($var[$type][$field])) {
    return '  width: '. $var[$type][$field]['width'] .'px;';
  }
  else {
    return '  width: 100px;';
  }
}

/**
 * helper function imagefield_gallery_lightbox2_css_render()
 */

function theme_lightbox2_imagefield_gallery_css_height($type, $field) {
  $var = variable_get('lightbox2_imagefield_gallery_css', array());
  if (isset($var[$type][$field])) {
    return '  height: '. $var[$type][$field]['height'] .'px;';
  }
  else {
    return '  height: 100px;';
  }
}
