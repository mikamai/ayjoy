<?php
// $Id: importexportapi_menu.inc,v 1.3 2006/09/19 02:41:58 jaza Exp $

/**
 * @file
 * Implements data definitions on behalf of menu.module.
 */

/**
 * Implementation of hook_def().
 */
function menu_def() {
  $defs = array();

  $def = array(
    '#type' => 'entity',
    '#title' => t('Menu item'),
    '#db_default_table' => 'menu',
    '#xml_plural' => 'menu-items',
    '#csv_plural' => 'menu-items'
  );

  $def['mid'] = array(
    '#type' => 'int',
    '#title' => t('Menu item ID'),
    '#key' => TRUE,
    '#db_uses_sequences' => TRUE
  );
  $def['pid'] = array(
    '#type' => 'int',
    '#title' => t('Parent menu item ID'),
    '#reference_entity' => 'menu_item',
    '#reference_field' => array('mid'),
    '#reference_parent' => FALSE
  );
  $def['path'] = array(
    '#title' => t('System path'),
    '#xml_mapping' => 'system-path',
    '#csv_mapping' => 'system-path'
  );
  $def['title'] = array(
    '#title' => t('Title')
  );
  $def['description'] = array(
    '#title' => t('Description')
  );
  $def['weight'] = array(
    '#type' => 'int',
    '#title' => t('Weight')
  );
  $def['type'] = array(
    '#type' => 'int',
    '#title' => t('Type')
  );

  $defs['menu_item'] = $def;

  return $defs;
}

/**
 * Implementation of hook_db_def_tables().
 */
function menu_db_def_tables() {
  $tables = array();

  $tables['menu'] = 'm';

  return $tables;
}
