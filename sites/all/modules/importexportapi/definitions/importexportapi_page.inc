<?php
// $Id: importexportapi_page.inc,v 1.7 2006/08/29 04:33:45 jaza Exp $

/**
 * @file
 * Implements data definitions on behalf of page.module.
 */

/**
 * Implementation of hook_def().
 */
function page_def() {
  $defs = array();

  $type = 'page';
  $def = importexportapi_node_get_def($type);

  $type_info = module_invoke($type, 'node_info');
  $type_info = $type_info[$type];
  $def['#title'] = $type_info['name'];
  $def['#title'][0] = strtoupper($def['#title'][0]);

  $def['#xml_plural'] = $def['#csv_plural'] = 'pages';
  $def['revisions']['#csv_plural'] = 'page-revisions';

  $def['type']['#db_filter'] = array(
    'values' => array($type)
  );

  $defs['page'] = $def;

  return $defs;
}
