<?php
// $Id: importexportapi_story.inc,v 1.6 2006/08/29 04:33:45 jaza Exp $

/**
 * @file
 * Implements data definitions on behalf of story.module.
 */

/**
 * Implementation of hook_def().
 */
function story_def() {
  $defs = array();

  $type = 'story';
  $def = importexportapi_node_get_def($type);

  $type_info = module_invoke($type, 'node_info');
  $type_info = $type_info[$type];
  $def['#title'] = $type_info['name'];
  $def['#title'][0] = strtoupper($def['#title'][0]);

  $def['#xml_plural'] = $def['#csv_plural'] = 'stories';
  $def['revisions']['#csv_plural'] = 'story-revisions';

  $def['type']['#db_filter'] = array(
    'values' => array($type)
  );

  $defs['story'] = $def;

  return $defs;
}
