$Id: README.txt,v 1.3 2007/05/18 15:06:19 darrenoh Exp $

Drupal assigns each module a weight. For most operations involving any
module that defines a particular hook, the modules are invoked in order
first by weight, then by name.

This module adds a weight column to the modules table at
admin/build/modules, allowing weights to be viewed and edited. Once
activated, a weight column appears on the modules table. To change a module
weight, edit its value and press "Save configuration". Any user who can
submit the admin/build/modules form will be able to change module weights.
