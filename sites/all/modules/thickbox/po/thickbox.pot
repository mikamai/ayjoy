# $Id: thickbox.pot,v 1.1.2.3 2008/01/05 11:06:56 frjo Exp $
#
# LANGUAGE translation of Drupal (thickbox.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  thickbox.module,v 1.9.2.7 2007/09/30 07:14:38 frjo
#  thickbox.info,v 1.2.2.2 2007/06/16 10:11:28 frjo
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2008-01-05 11:55+0100\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: thickbox.module:17
msgid "Thickbox options"
msgstr ""

#: thickbox.module:21
msgid "Enable for image nodes"
msgstr ""

#: thickbox.module:23
msgid "Automatically activate Thickbox for all image nodes (requires the image module)."
msgstr ""

#: thickbox.module:33
msgid "Image derivative"
msgstr ""

#: thickbox.module:36
msgid "Select which image derivative will be loaded."
msgstr ""

#: thickbox.module:41
msgid "Enable for login links"
msgstr ""

#: thickbox.module:43
msgid "Automatically activate Thickbox for links to user/login."
msgstr ""

#: thickbox.module:47
msgid "Deactivate Thickbox on specific pages"
msgstr ""

#: thickbox.module:49
msgid "Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page."
msgstr ""

#: thickbox.module:66 thickbox.info:0
msgid "Thickbox"
msgstr ""

#: thickbox.module:67
msgid "Configure Thickbox behavior."
msgstr ""

#: thickbox.module:75
msgid "Login"
msgstr ""

#: thickbox.module:116
msgid "Close"
msgstr ""

#: thickbox.module:117
msgid "Next >"
msgstr ""

#: thickbox.module:118
msgid "< Prev"
msgstr ""

#: thickbox.module:119
msgid "or Esc Key"
msgstr ""

#: thickbox.module:120
msgid "Next / Close on last"
msgstr ""

#: thickbox.module:121
msgid "Image !current of !total"
msgstr ""

#: thickbox.module:0
msgid "thickbox"
msgstr ""

#: thickbox.info:0
msgid "Enables Thickbox, a jQuery plugin."
msgstr ""

