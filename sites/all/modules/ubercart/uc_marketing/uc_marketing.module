<?php

function uc_marketing_help($section) {
  $output = '';
  switch ($section) {
    case 'admin/store/settings/marketing/edit/special':
      $output .= t('Mark products as "Sticky" in the node publication area to display them in the "Special offers" block.');
    break;
  }
  return $output;
}

function uc_marketing_menu($may_cache) {
  $items = array();
  
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/store/settings/marketing',
      'access' => user_access('administer store'),
      'title' => t('Marketing settings'),
      'callback' => 'uc_marketing_admin_settings_overview',
      'type' => MENU_NORMAL_ITEM,
    );
    $items[] = array(
      'path' => 'admin/store/settings/marketing/view',
      'title' => t('Overview'),
      'weight' => -5,
      'type' => MENU_DEFAULT_LOCAL_TASK,
    );
    $items[] = array(
      'path' => 'admin/store/settings/marketing/edit',
      'title' => t('Edit'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('uc_marketing_visual_settings'),
      'type' => MENU_LOCAL_TASK,
    );
    $items[] = array(
      'path' => 'admin/store/settings/marketing/edit/visual',
      'title' => t('Visual catalog'),
      'weight' => -5,
      'type' => MENU_DEFAULT_LOCAL_TASK,
    );
    $items[] = array(
      'path' => 'admin/store/settings/marketing/edit/special',
      'title' => t('Special offers'),
      'weight' => -2,
      'callback' => 'drupal_get_form',
      'callback arguments' => array('uc_marketing_special_settings'),
      'type' => MENU_LOCAL_TASK,
    );
    $items[] = array(
      'path' => 'admin/store/settings/marketing/edit/bestsellers',
      'title' => t('Best sellers'),
      'weight' => 2,
      'callback' => 'drupal_get_form',
      'callback arguments' => array('uc_marketing_bestsellers_settings'),
      'type' => MENU_LOCAL_TASK,
    );
    $items[] = array(
      'path' => 'admin/store/settings/marketing/edit/contact',
      'title' => t('Contact'),
      'weight' => 5,
      'callback' => 'drupal_get_form',
      'callback arguments' => array('uc_marketing_contact_settings'),
      'type' => MENU_LOCAL_TASK,
    );
  }
  
  return $items;
}

function uc_marketing_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks[1]['info'] = variable_get('uc_visual_block_catalog_name', t('Visual catalog'));
      $blocks[2]['info'] = variable_get('uc_marketing_special_block_title', t('Special offers'));
      $blocks[3]['info'] = variable_get('uc_marketing_bestsellers_block_title', t('Best sellers'));
      $blocks[4]['info'] = t('Contact link');
    return $blocks;
    case 'view':
      $block = array();
      if (user_access('view catalog')) {
        switch ($delta) {
          case 1:
            $block['subject'] = variable_get('uc_catalog_name', t('Catalog'));
            $block['content'] = theme('uc_catalog_browse');
          break;
          case 2:
            // no title, it takes it from the theme function
            $block['content'] = theme('uc_marketing_special_products');
          break;
          case 3:
            // no title, it takes it from the theme function
            $block['content'] = theme('uc_marketing_bestsellers_products');
          break;
          case 4:
            // no title, we just don't want one
            $block['content'] = theme('uc_marketing_contact_link');
          break;
        }
      }
    return $block;
  }
}

function uc_marketing_admin_settings_overview() {
  $sections = array();
  
  $sections[] = array(
    'edit' => 'admin/store/settings/marketing/edit/visual',
    'title' => t('Visual catalog'),
    'items' => array(
      t('The title for the Visual Catalog block is %title.', array('%title' => variable_get('uc_visual_block_catalog_name', t('Visual catalog')))),
    ),
  );
  $sections[] = array(
    'edit' => 'admin/store/settings/marketing/edit/special',
    'title' => t('Special offers'),
    'items' => array(
      format_plural(variable_get('uc_marketing_special_block_lines', 1),
        t('If grid view is selected, the "Special offers" block will display @count line.'),
        t('If grid view is selected, the "Special offers" block will display @count lines.')),
      t('The title for the "Special offers" block is %title.', array('%title' => variable_get('uc_marketing_special_block_title', t('Special offers')))),
      variable_get('uc_marketing_special_block_help_allowed', TRUE) ?
        t('Help text will be displayed below the "Special offers" block.') :
        t('Help text will not be displayed below the "Special offers" block.'),
      t('The help text for the "Special offers" block is: %help', array('%help' => variable_get('uc_marketing_special_block_help', t("We're always looking for new ways to bring our products to the broadest audience. Here are our special offers for this month.")))),
    ),
  );
  $sections[] = array(
    'edit' => 'admin/store/settings/marketing/edit/best_sellers',
    'title' => t('Best sellers'),
    'items' => array(
      format_plural(variable_get('uc_marketing_bestsellers_block_lines', 1),
        t('If grid view is selected, the "Best sellers" block will display @count line.'),
        t('If grid view is selected, the "Best sellers" block will display @count lines.')),
      t('The title for the "Best sellers" block is %title.', array('%title' => variable_get('uc_marketing_bestsellers_block_title', t('Best sellers')))),
      variable_get('uc_marketing_bestsellers_block_help_allowed', TRUE) ?
        t('Help text will be displayed below the "Best sellers" block.') :
        t('Help text will not be displayed below the "Best sellers" block.'),
      t('The help text for the "Best sellers" block is: %help', array('%help' => variable_get('uc_marketing_bestsellers_block_help', t("So many people can't be wrong. These products have been ordered the most.")))),
    ),
  );
  $sections[] = array(
    'edit' => 'admin/store/settings/marketing/edit/contact',
    'title' => t('Contact'),
    'items' => array(
      t('The url for the contact form is %url.', array('%url' => variable_get('uc_marketing_contact_url', 'contact'))),
    ),
  );
  
  $output = theme('uc_settings_overview', $sections);
  return $output;
}

function uc_marketing_visual_settings() {
  $form = array();
  
  // JTR - elements for the visual catalog block view administration
  $form['block-visual-display'] = array('#type' => 'fieldset',
    '#title' => t('Independent catalog blocks display'),
    '#collapsible' => true,
    '#collapsed' => false,
    '#weight' => -6,
    '#attributes' => array('class' => 'block-display'),
  );
  $form['block-visual-display']['uc_visual_block_catalog_name'] = array('#type' => 'textfield',
    '#title' => t('Title for the block catalog'),
    '#default_value' => variable_get('uc_visual_block_catalog_name', t('Visual catalog')),
    '#weight' => -4,
    '#description' => t("Customize here the title for the independent catalog block."),
  );
  
  return system_settings_form($form);
}

function uc_marketing_special_settings() {
  $form = array();
  
  $form['uc_marketing_special_block_lines'] = array('#type' => 'select',
    '#title' => t('Number of lines'),
    '#default_value' => variable_get('uc_marketing_special_block_lines', 1),
    '#options' => drupal_map_assoc(uc_range(1, 4)),
    '#weight' => -4,
    '#description' => t('Number of lines in "Special offer" block if grid view selected.'),
  );
  
  // JTR - elements for special offers block administration
  $form['special-block-display'] = array('#type' => 'fieldset',
    '#title' => t('Special offers block display'),
    '#collapsible' => true,
    '#collapsed' => false,
    '#weight' => -4,
    '#attributes' => array('class' => 'block-display'),
  );
  $form['special-block-display']['uc_marketing_special_block_title'] = array('#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get('uc_marketing_special_block_title', t('Special offers')),
    '#weight' => -8,
    '#description' => t("Customize here the title to appear on that block."),
  );
  $form['special-block-display']['uc_marketing_special_block_help_allowed'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display help text'),
    '#weight' => -4,
    '#default_value' => variable_get('uc_marketing_special_block_help_allowed', true),
    '#description' => t("It will display a help text of your choice below the block."),
  );
  $form['special-block-display']['uc_marketing_special_block_help'] = array('#type' => 'textarea',
    '#title' => t('Help text'),
    '#default_value' => variable_get('uc_marketing_special_block_help', t("We're always looking for new ways to bring our products to the broadest audience. Here are our special offers for this month.")),
    '#weight' => -1,
    '#description' => t("Customize here the text to appear below that block where you can explain some conditions to it."),
  );

  return system_settings_form($form);
}

function uc_marketing_bestsellers_settings() {
  $form = array();
  
  $form['uc_marketing_bestsellers_block_lines'] = array('#type' => 'select',
    '#title' => t('Number of lines'),
    '#default_value' => variable_get('uc_marketing_bestsellers_block_lines', 1),
    '#options' => drupal_map_assoc(uc_range(1, 4)),
    '#weight' => -1,
    '#description' => t("Number of lines in best sellers products block if grid view selected."),
  );
  
  // JTR - elements for best sellers block administration
  $form['bestsellers-block-display'] = array('#type' => 'fieldset',
    '#title' => t('Best sellers block display'),
    '#collapsible' => true,
    '#collapsed' => false,
    '#weight' => -1,
    '#attributes' => array('class' => 'block-display'),
  );
  $form['bestsellers-block-display']['uc_marketing_bestsellers_block_title'] = array('#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get('uc_marketing_bestsellers_block_title', t('Best Sellers')),
    '#weight' => -8,
    '#description' => t("Customize here the title to appear on that block."),
  );
  $form['bestsellers-block-display']['uc_marketing_bestsellers_block_help_allowed'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display help text'),
    '#weight' => -4,
    '#default_value' => variable_get('uc_marketing_bestsellers_block_help_allowed', true),
    '#description' => t("It will display a help text of your choice bellow the block."),
  );
  $form['bestsellers-block-display']['uc_marketing_bestsellers_block_help'] = array('#type' => 'textarea',
    '#title' => t('Help'),
    '#default_value' => variable_get('uc_marketing_bestsellers_block_help', t("So many people can't be wrong. These products have been ordered the most.")),
    '#weight' => -1,
    '#description' => t("Customize here the text to appear below that block where you can explain some conditions to it."),
  );

  return system_settings_form($form);
}

function uc_marketing_contact_settings() {
  $form = array();
  
  $form['uc_marketing_consult_block'] = array('#type' => 'checkbox',
    '#title' => t('Display link to catalog consult form at bottom'),
    '#default_value' => variable_get('uc_marketing_consult_block', FALSE),
    '#weight' => 1,
    '#description' => t("These products appear on that block when the site has some purchases available. Beware that if there are not products on orders no products may appear which means an empty block with a non available data message."),
  );
  $form['uc_marketing_contact_url'] = array('#type' => 'textfield',
    '#title' => t('More information form URL'),
    '#description' => t("The path to the page with a form you could have created to handle consults about products. Do not include a trailing slash."),
    '#default_value' => variable_get('uc_marketing_contact_url', 'contact'),
    '#weight' => 2,
  );
  
  return system_settings_form($form);
}

/**
 * Display the content for special offer products list
 * JTR
 */
function theme_uc_marketing_special_products() {
  $product_types = module_invoke_all('product_types');
  $gridwidth = variable_get('uc_catalog_grid_display_width', 3);
  if (variable_get('uc_catalog_grid_display', false)) {
    $gridlines = variable_get('uc_marketing_special_block_lines', 1);
    $gridwidth = $gridwidth * $gridlines;
  }

  $products = array();
  $sql = "SELECT DISTINCT(n.nid), n.title, p.sell_price
    FROM {node} n
      INNER JOIN {uc_products} AS p ON n.nid = p.nid
    WHERE n.status = 1
      AND n.sticky = 1
      AND n.type IN ('". implode("','", $product_types) ."') 
    ORDER BY p.sell_price ASC";

  $result = db_query_range($sql, 0, $gridwidth);
  while ($node = db_fetch_object($result)) {
    $products[] = $node->nid;
  }

  if (count($products)) {
    $output .= '<h2 class="title special">' . variable_get('uc_marketing_special_block_title', t('Special Offers')) . '</h2>';
    $output .= theme('uc_catalog_products', $products);
    if (variable_get('uc_marketing_special_block_help_allowed', true)) {
      $output .= '<p class="special description">' . variable_get('uc_marketing_special_block_help', t("We're always looking for new ways to bring our products to the broadest audience. Here are our special offers for this month.")) . '</p>';
    }
  }
  return $output;
}

/**
 * Display the content for best sellers products list
 * JTR
 */
function theme_uc_marketing_bestsellers_products() {
  $product_types = module_invoke_all('product_types');
  $gridwidth = variable_get('uc_catalog_grid_display_width', 3);
  if (variable_get('uc_catalog_grid_display', false)) {
    $gridlines = variable_get('uc_marketing_bestsellers_block_lines', 1);
    $gridwidth = $gridwidth * $gridlines;
  }

  $products = array();
  $sql = "SELECT nd.nid, nd.title,
      (SELECT SUM(op.qty)
        FROM {uc_order_products} AS op
        WHERE op.nid = nd.nid) AS sold
    FROM {node} AS nd
    WHERE nd.type IN ('". implode("','", $product_types) ."')
      AND nd.status = 1
    ORDER BY sold DESC";
  $result = db_query_range($sql, 0, $gridwidth);
  while ($node = db_fetch_object($result)) {
    $products[] = $node->nid;
  }

  if (count($products)) {
    $output .= '<h2 class="title best-sellers">' . variable_get('uc_marketing_bestsellers_block_title', t('Best Sellers')) . '</h2>';
    $output .= theme('uc_catalog_products', $products);
    if (variable_get('uc_marketing_bestsellers_block_help_allowed', true)) {
      $output .= '<p class="best-sellers description">' . variable_get('uc_marketing_bestsellers_block_help', t("So many people can't be wrong. These products have been ordered the most.")) . '</p>';
    }
  }
  return $output;
}

function theme_uc_marketing_contact_link() {
  $contactform = variable_get('uc_marketing_contact_url', 'contact');
  return '<p class="not-found">'. t("Can't find what you are looking for?") .' '. l(t('Contact us!'), $contactform) .'</p>';
}
