<?php
	  /* User orders */
  	  global $user;
      $uid = $user->uid;
      $orders = getUserOrders($uid);

      /* User cart */
      $items = uc_cart_get_contents($user->uid);

      	$count_products = 0;
      	$price = 0;
      	foreach ($items as $product) {
      		$product_node = node_load($product->nid);
      		$count_products += $product->qty;
      		$price += $product_node->sell_price * $product->qty;
      	}
?>


   <div id="your-room">
        <h2  class="colored-header"><? print t('YOUR ROOM') ?></h2>
    </div>
    <div id="your-room-box-down">
      <div id="your-room-box-up">
      <?php   ?>


	<div id="cart">
	  <p class="room-up"><?php print l(t('Cart'),'cart')?></p>
	  		<?php print l('<img src="' . base_path() . path_to_theme() . '/css/images/cart2.png"/>','cart',array(),null,null,null,true)?>
			<?php print l($count_products.' '.format_plural($count_products,t('item'),t('items')),'cart') ?><br /><span class="gray"><?php print uc_currency_format($price) ?></span>
		</p>
	</div>


   <div id="orders">
   	  <p class="room-up">

		<?php if ($orders) : ?>
		   	<?php foreach($orders as $status => $count) : ?>
		   		<?php print l(t('Orders'),'user/'.$user->uid.'/orders') ?>
		   		 <br />
		   	<?php endforeach; ?>
		<?php else : ?>
			<?php print l(t('Orders'),'user/'.$user->uid.'/orders')?>
		<?php endif;?>

   	  </p>

	  <a class="active" href="<?php print base_path() . 'user/'.$user->uid.'/orders' ?>">
	    <img src="<?php print base_path() . path_to_theme() ?>/css/images/orders2.png" />
	  </a>
	<p>
	 <?php if ($orders) : ?>
	   	<?php foreach($orders as $status => $count) : ?>
	   		<?php print l($count.' '. t($status),'user/'.$user->uid.'/orders') ?>
	   		 <br />
	   	<?php endforeach; ?>
	<?php else : ?>
		<?php print l('0'.' '.t('Orders'),'user/'.$user->uid.'/orders')?>
	<?php endif;?>
    </p>
    </div>


    <div id="forum">
		  <p class="room-up"><a class="active" href="/forum"><? print t('Forum') ?></a></p>
		  <a class="active" href="/forum">
		    <img src="<?php print base_path() . path_to_theme() ?>/css/images/forum2.png" />
		  </a>
		<p>
        <?php
			$interval = time() - 900;
			$anonymous_count = sess_count($interval);
			$authenticated_users = db_query('SELECT u.uid, u.name FROM {users} u INNER JOIN {sessions} s ON u.uid = s.uid WHERE s.timestamp >= %d AND s.uid > 0 ORDER BY s.timestamp DESC', $interval);
			$authenticated_count = db_num_rows($authenticated_users);
			if ($anonymous_count == 1 && $authenticated_count == 1) {
			  $link = t('%members', array('%members' => format_plural($authenticated_count, '1 user', '@count users'), '%visitors' => format_plural($anonymous_count, '1 guest', '@count guests')));
			  $output = l(strip_tags($link),'forum');
			}
			else {
			  $link = t('%members', array('%members' => format_plural($authenticated_count, '1 user', '@count users'), '%visitors' => format_plural($anonymous_count, '1 guest', '@count guests')));
			  $output = l(strip_tags($link),'forum');

			}
			print $output;
		?>
		</p>
		</div>
          <br class="clear" />
      </div>
    </div>