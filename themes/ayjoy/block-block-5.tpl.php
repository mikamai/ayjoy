<?php global $user; ?>

<div class="blocco_your_room">
       <h2 class="arrow"> <?php print t('YOUR ROOM') ?> </h2>
       <ul class="links_room">
               <li> <? print l(t('YOUR CART'), 'cart') ?> </li>
               <li> <? print l(t('YOUR ORDERS'), 'user/'.$user->uid.'/orders') ?> </li>
               <li> <? print l(t('EDIT YOUR PROFILE'), 'user/'.$user->uid.'/edit') ?> </li>
               <li class="forum"> <? print l(t('FORUM'), 'forum') ?> </li>               
       </ul>
</div>
 
