<?php
	  /* User orders */
  	  global $user; 
      $uid = $user->uid;
      $orders = getUserOrders($uid);
      
      /* User cart */
      $items = uc_cart_get_contents($user->uid);
      	
      	$count_products = 0;
      	$price = 0;
      	foreach ($items as $product) {
      		$product_node = node_load($product->nid);
      		$count_products += $product->qty;
      		$price += $product_node->sell_price * $product->qty;
      	}
?>

<div id="unlogged_cart">
	
	<p>
		<?php print l($count_products.' '.format_plural($count_products,t('item'),t('items')),'cart') ?>&nbsp<?php print uc_currency_format($price) ?>
	</p>
</div>
