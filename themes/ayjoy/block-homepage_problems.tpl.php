<div id="articoli_list">

<h1 class="titolo"><?php print t('Problems and solutions') ?> </h1>

<?php foreach ($nodes as $n) : ?>
<?php $node = node_load($n->nid); ?>

<div class="un_terzo">
    <h2 class="titolo"> <?php echo l($node->title,"node/$node->nid") ?> </h2>
    <div class="content_articolo"> 
    <?php  print truncate_text($node->teaser,70) ?>
    </div>

     
</div>
<?php endforeach; ?>
<br clear="all" />
</div>
