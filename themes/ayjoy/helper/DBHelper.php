<?php

function countTypeFromUser($uid, $type)
{
  $utente = user_load(array('uid' => $uid));
  if ($utente)
  {
      $count = db_result(db_query("SELECT COUNT(*) FROM {node} WHERE type LIKE '%s%' AND uid = '%d'", $type,$uid));
     
      return $count;
  }
  return FALSE;
}


function get_last_node_comment($node) {
	$comments_db = db_query('SELECT * FROM {comments} WHERE nid = %d AND status = %d ORDER BY cid DESC LIMIT 0,1', $node->nid, COMMENT_PUBLISHED);
	while ($comment = db_fetch_object($comments_db)) {
	    $comments[] = $comment;
	}
	if (!empty($comments)) {
	    $comment = $comments[0];
	    return $comment;
	}
	return FALSE;
}
?>
