<?php
function beautify_date($timestamp = NULL) {
  if (date("Ymd") == date("Ymd", $timestamp)) {
      return "Oggi";
  }
  else {
      return(t(date("l", $timestamp)) . " " . date("j", $timestamp) . " " . t(date("F", $timestamp)));
  }
}
		
function beautify_date_and_time($timestamp = NULL) {
      return beautify_date($timestamp) . ", Ore " . date("H:i");
 }
 
 ?>