<?php
function menu_tree_raw($pid = 1) {
  $menu = menu_get_menu();
  $output = '';

  if (isset($menu['visible'][$pid]) && $menu['visible'][$pid]['children']) {
    foreach ($menu['visible'][$pid]['children'] as $mid) {
      $type = isset($menu['visible'][$mid]['type']) ? $menu['visible'][$mid]['type'] : NULL;
      $children = isset($menu['visible'][$mid]['children']) ? $menu['visible'][$mid]['children'] : NULL;
      $links[] = theme('menu_item_raw', $mid, menu_in_active_trail($mid));
      print menu_in_active_trail_in_submenu($mid, $pid);
    }
  }
  
  
  return $links;
}

function ayjoy_menu_item_raw($mid, $children = '', $leaf = TRUE) {
  return menu_item_link_active($mid,FALSE);
}

/**
 * Returns the rendered link to a menu item.
 *
 * @param $mid
 *   The menu item id to render.
 * @param $theme
 *   Whether to return a themed link or the link as an array
 */
function menu_item_link_active($mid, $theme = TRUE) {
  $item = menu_get_item($mid);
  $link_item = $item;
  $link = '';

  while ($link_item['type'] & MENU_LINKS_TO_PARENT) {
    $link_item = menu_get_item($link_item['pid']);
  }

  if ($theme) {
    $link = theme('menu_item_link', $item, $link_item);
  }
  else {
    $link = array(
      'title' => $item['title'],
      'href' => $link_item['path'],
      'attributes' => !empty($item['description']) ? array('title' => $item['description']) : array()
    );
  }

  return $link;
}

function drupal_get_form_raw($form_id) {
  // In multi-step form scenarios, the incoming $_POST values are not
  // necessarily intended for the current form. We need to build
  // a copy of the previously built form for validation and processing,
  // then go on to the one that was requested if everything works.

  $form_build_id = md5(mt_rand());
  if (isset($_POST['form_build_id']) && isset($_SESSION['form'][$_POST['form_build_id']]['args']) && $_POST['form_id'] == $form_id) {
    // There's a previously stored multi-step form. We should handle
    // IT first.
    $stored = TRUE;
    $args = $_SESSION['form'][$_POST['form_build_id']]['args'];
    $form = call_user_func_array('drupal_retrieve_form', $args);
    $form['#build_id'] = $_POST['form_build_id'];
  }
  else {
    // We're coming in fresh; build things as they would be. If the
    // form's #multistep flag is set, store the build parameters so
    // the same form can be reconstituted for validation.
    $args = func_get_args();
    $form = call_user_func_array('drupal_retrieve_form', $args);
    if (isset($form['#multistep']) && $form['#multistep']) {
      // Clean up old multistep form session data.
      _drupal_clean_form_sessions();
      $_SESSION['form'][$form_build_id] = array('timestamp' => time(), 'args' => $args);
      $form['#build_id'] = $form_build_id;
    }
    $stored = FALSE;
  }

  // Process the form, submit it, and store any errors if necessary.
  drupal_process_form($args[0], $form);

  if ($stored && !form_get_errors()) {
    // If it's a stored form and there were no errors, we processed the
    // stored form successfully. Now we need to build the form that was
    // actually requested. We always pass in the current $_POST values
    // to the builder function, as values from one stage of a multistep
    // form can determine how subsequent steps are displayed.
    $args = func_get_args();
    $args[] = $_POST;
    $form = call_user_func_array('drupal_retrieve_form', $args);
    unset($_SESSION['form'][$_POST['form_build_id']]);
    if (isset($form['#multistep']) && $form['#multistep']) {
      $_SESSION['form'][$form_build_id] = array('timestamp' => time(), 'args' => $args);
      $form['#build_id'] = $form_build_id;
    }
    drupal_prepare_form($args[0], $form);
  }

  return $form;
}


?>