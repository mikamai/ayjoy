<?php
function getAllProducts() {
	$products = array();
	//$sql = "SELECT n.title from {uc_products} p JOIN {node} node ON n.nid=p.nid LEFT JOIN {i18n_node} i18n ON node.nid = i18n.nid WHERE 'i18n.language ='it' OR i18n.language ='' OR i18n.language IS NULL";
	
	$sql = "SELECT n.title, n.nid FROM {node} n WHERE n.type = 'product' AND n.status = 1";
	$sql_rewrite = i18n_db_rewrite_sql($sql, 'n', '');
	
	$products_query = db_query(db_rewrite_sql($sql,'n', 'nid', $sql_rewrite));
	
	while($product = db_fetch_object($products_query)) {
		$products[$product->nid] = $product->title;
	}
	//var_dump($products);
	return $products;
}
?>