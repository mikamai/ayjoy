<?php
 
function getUserOrders($uid = NULL) {
      $order_print = array();
      $order_stat = array();
      $orders = array();	

      $order = uc_order_load($uid); 

      $result = db_query("SELECT o.order_id, o.created, os.title, SUM(op.qty) AS products, o.order_total AS total FROM {uc_orders} AS o LEFT JOIN {uc_order_statuses} AS os ON o.order_status = os.order_status_id LEFT JOIN {uc_order_products} AS op ON o.order_id = op.order_id WHERE o.uid = %d AND o.order_status IN ". uc_order_status_list('general', TRUE) ." GROUP BY o.order_id, o.created, os.title, o.order_total", $uid);
      while ($order = db_fetch_object($result)) {
      		$orders[] = $order;	
      }
      
      /* Rebuild with states */
      foreach ($orders as $order) {
		  $order_stat[$order->title][] = $order;
      }      
      
      foreach ($order_stat as $state => $values) {
      	$order_print[$state] = count($values);
      }
      return $order_print;
}

?>
