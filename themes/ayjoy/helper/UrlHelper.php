<?php

function lspan($text, $path, $attributes = array(), $query = NULL, $fragment = NULL, $absolute = FALSE, $html = FALSE) {
  if (($path == $_GET['q']) || ($path == '<front>' && drupal_is_front_page())) {
    if (isset($attributes['class'])) {
      $attributes['class'] .= ' active';
    }
    else {
      $attributes['class'] = 'active';
    }
  }
  return '<a href="'. check_url(url($path, $query, $fragment, $absolute)) .'"'. drupal_attributes($attributes) .'>'. ($html ? '<span>'.$text.'</span>' : '<span>'.check_plain($text).'</span>') .'</a>';
}


function getnid(){
    $path = drupal_lookup_path('source', $_GET['q']);
    if($path == false){
        $path = $_GET['q'];
    }
    $pathvars = explode('/', $path);
    $nid = $pathvars[0];
    return $nid;   
}

?>
