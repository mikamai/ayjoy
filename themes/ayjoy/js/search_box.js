$(document).ready(function() {
	$('#edit-filter0').selectbox();
	$('#edit-filter2').selectbox();
	$('#edit-filter4').selectbox();

	$("#edit-filter0_container").click(function()
	{ 
		var select = $("#edit-filter0").val();
		if (select != '**ALL**') {
			$("#views-filters").submit();
		}		
	}); 
	$("#edit-filter2_container").click(function()
	{ 
		var select = $("#edit-filter2").val();
		if (select != '**ALL**') {
			$("#views-filters").submit();
		}		
	});
	$("#edit-filter4_container").click(function()
		{ 
			var select = $("#edit-filter4").val();
			if (select != '**ALL**') {
				$("#views-filters").submit();
			}		
	});

});
