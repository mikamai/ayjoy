<?php $product = $node;
  $product = node_load($node->nid);
?>
 <div class="product-item">
    <div class="pruduct-item-image">
     
     <?php
      $titlelink = l($product->title, "node/".$node->nid, array(), null, null, false, true);
	  if (module_exists('imagecache') && isset($product->field_image_cache) && file_exists($product->field_image_cache[0]['filepath'])) {
    	  print l(theme('imagecache', 'product_list', $product->field_image_cache[0]['filepath']), "node/".$node->nid, array(), null, null, false, true);
	  }
     ?>
    </div>

   <!-- 
    <div class="product-item-desc">
      <h3> <?php print $titlelink ?> </h3>
      <p>Prima descrizione del prodotto a destra dell'immagine</p>
    </div>
    -->
    
    <br class="clear">
    <div class="product-item-desc-2">
          <h3> <?php print $titlelink ?> </h3>
    
 	<p><?php print truncate_html($product->body, 255) ?> </p>	
      <a aref="#"><?php print l(t('Read More'),'node/'.$product->nid) ?></a></p>
    </div>
    

    <div>
    <!-- Ora la barra con i dati carrello -->
    <div id="product-item-cart-down">
      <div id="product-item-cart-up">
        <ul>
          <li class="cart-image"> <?php print theme('image',path_to_theme().'/css/images/cart-2.png') ?></li>      
        
          <li class="price"><span class="price"><?php print uc_currency_format($product->sell_price) ?></span></li>
          <li class="buy-link">
            <?php  
            	print theme('uc_product_add_to_cart_blocco_catalogo', $product); 
            ?>
            
          </li>
          <br class="clear" />
        </ul>
      </div>
    </div>
  </div>
  </div>
  
