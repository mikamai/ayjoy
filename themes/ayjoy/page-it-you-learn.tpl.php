<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>" lang="<?php print $language ?>">
  <head>
    <title><?php print $head_title ?> | Vendita integratori, Vendita integratori online | Integratori alimentari</title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    <!--[if lte IE 6]>
      <link rel="stylesheet" href="<?php print base_path() . path_to_theme() ?>/css/ie6.css" type="text/css" media="screen"/>
    <![endif]-->
    <!--[if IE 7]>
      <link rel="stylesheet" href="<?php print base_path() . path_to_theme() ?>/css/ie7.css" type="text/css" media="screen"/>
    <![endif]-->
    <link rel="shortcut icon" href="/themes/ayjoy/css/images/favicon.png" type="image/x-icon" />
</head>

<!-- body id unico -->
<?php
$uri_path = trim($_SERVER['REQUEST_URI'], '/');
// Split up the remaining URI into an array, using '/' as delimiter.
$uri_parts = explode('/', $uri_path);

// If the first part is empty, label both id and class 'main'.
if ($uri_parts[0] == '') {
    $body_id = 'home';
    $body_class = 'main';
}

/* override the locale uri path */
else if (array_key_exists($uri_parts[0],i18n_language_list()) && (empty($uri_parts[1]))) {
	$body_id = 'home';
    $body_class = 'main';
}


else {
    // Construct the id name from the full URI, replacing slashes with dashes.
    $body_id = str_replace('/','-', $uri_path);
    // Construct the class name from the first part of the URI only.
    $body_class = $uri_parts[0];
}

/**
* Add prefixes to create a kind of protective namepace to prevent possible
* conflict with other css selectors.
*
* - Prefix body ids with "page-"(since we use them to target a specific page).
* - Prefix body classes with "section-"(since we use them to target a whole sections).
*/
$body_id = ''.$body_id;
$body_class = 'section-'.$body_class;

print "<body class=\"learn-body\" id=\"$body_id\"";
print theme('onload_attribute');
print ">";
?>


<!-- Layout -->
  	  <div id="container">
	    <div id="header">
        <div id="logo">
          <h1><a href="/" title="return homepage">Ayjoy Your Natural Healthcarea</a></h1>
        </div>
   		<?php print $logout ?>
   	   	<?php print $loginbox ?>

	      <div id="top-menu-down">
	        <div id="top-menu-up">
	          <div  id="main-page">
	            <ul>
	              <li><a href="<?php print base_path() ?>" title="">Home</a></li>
	            </ul>
	          </div>
	          <div  id="navigation">
	      			  <?php if (isset($primary_links)) : ?>
						  <?php print theme('menu_links', $primary_links) ?>
			          <?php endif; ?>
	          </div>
	          <div  id="search">
	           <h2 class="colored-header"><? print t('search') ?></h2>
			<?php print theme('search_box_header') ?>          
	           </div>
	          
				<?php print $language_drop ?>
	
				<!--div  id="help">
	            <ul>
	              <li><a href="#" title="">help</a></li>
	            </ul>
	          </div-->
			   <div  id="help">
	           		<h2><?php print strtolower(t('Language')) ?></h2>
	          </div>
	          <br class="clear" />
	        </div>
	      </div>
	    </div>
	    <div id="summary">
        <div id="one" class="buttom">
	        <?php print l(t('You Buy'),'catalog') ?>

        </div>
        <div id="two" class="buttom">
         	<?php print l(t('You Care'),'problems-and-solutions') ?>

        </div>
        <div id="three" class="buttom">
         	<?php print l(t('You learn'),'you-learn') ?>

        </div>
        <br class="clear" />
	    </div>
	
	
      <div id="wrapper">
        <div id="content">
			 <div id="content-wrapper">
				<?php if ($breadcrumb): print $breadcrumb; endif; ?>
	          
         <?php if ($tabs): print '<div class="tabs">'.$tabs .'</div>'; endif; ?>
			<?php print $content ?>
			 </div>
	      </div>
	</div>
      <div id="right_content">
   		 <div id="right-content-wrapper">
			<?php print $right_content ?>
		</div>	
      </div>
      <div id="left_content">
			<div id="left-content-wrapper">
		 		<?php print $left_content ?>
			</div>	
      </div>
      <div id="footer">
        <div id="footer-down">
          <div id="footer-up">
          <?php print $footer_area ?>
          <!-- 
            <ul>
              <li><a href="#" title="">Homes</a></li>
              <li><a href="#" title="">Content</a></li>
              <li><a href="#" title="">Disclaimer</a></li>
              <li><a href="#" title="">Copyrights</a></li>
              <li class="last"><a href="#" title="">Legal</a></li>
            </ul>
            -->            
          </div>
        </div>
      </div>
	  </div>
<?php print $closure ?>
	</body>
</html>
