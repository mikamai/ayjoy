<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>" lang="<?php print $language ?>">
  <head>
    <title>Vendita integratori, Vendita integratori online | Integratori alimentari | <?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    <!--[if lte IE 6]>
      <link rel="stylesheet" href="<?php print base_path() . path_to_theme() ?>/css/ie6.css" type="text/css" media="screen"/>
    <![endif]-->
    <!--[if IE 7]>
      <link rel="stylesheet" href="<?php print base_path() . path_to_theme() ?>/css/ie7.css" type="text/css" media="screen"/>
    <![endif]-->
    <link rel="shortcut icon" href="/themes/ayjoy/css/images/favicon.png" type="image/x-icon" />
</head>

<!-- body id unico -->

<?php
$uri_path = trim($_SERVER['REQUEST_URI'], '/');
// Split up the remaining URI into an array, using '/' as delimiter.
$uri_parts = explode('/', $uri_path);

// If the first part is empty, label both id and class 'main'.
if ($uri_parts[0] == '') {
    $body_id = 'home';
    $body_class = 'main';
}

/* override the locale uri path */
else if (array_key_exists($uri_parts[0],i18n_language_list()) && (empty($uri_parts[1]))) {
	$body_id = 'home';
    $body_class = 'main';
}

else {
    // Construct the id name from the full URI, replacing slashes with dashes.
    $body_id = str_replace('/','-', $uri_path);
    // Construct the class name from the first part of the URI only.
    $body_class = $uri_parts[0];
}

/**
* Add prefixes to create a kind of protective namepace to prevent possible
* conflict with other css selectors.
*
* - Prefix body ids with "page-"(since we use them to target a specific page).
* - Prefix body classes with "section-"(since we use them to target a whole sections).
*/
$body_id = ''.$body_id;
$body_class = 'section-'.$body_class;
print "<body class=\"$body_class\" id=\"$body_id\"";
print theme('onload_attribute');
print ">";
?>



