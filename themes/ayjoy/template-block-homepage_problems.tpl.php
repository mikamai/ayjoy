<div class="line-440">
<?php $i = 0; ?>
<?php foreach ($nodes as $n) : ?>
<?php $node = node_load($n->nid); ?>
<?php $i++; ?> 

<div class="problem-<?php print $i ?>" class="col-200 left ml-20">
	 	<? /*<p class="date">
        <?php echo beautify_date($node->created) ?>
      </p> */?>
    <h3> <?php echo l($node->title,"node/$node->nid") ?> </h3>
    <p><?php  print truncate_text($node->teaser,70) ?></p>
</div>

<?php endforeach; ?>
<br clear="all" />
</div>
