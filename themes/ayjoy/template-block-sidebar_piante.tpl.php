<div id="right-edutainement">
  <div class="wrapper">
        <h2 class="colored-header"><? print t('EDUTAINEMENT') ?></h2>
        <img src="/themes/ayjoy/css/images/edu.jpg"  title="edutainement"/>
        	<?php foreach ($nodes as $n) : ?>
			<?php $node = node_load($n->nid); ?>
				<p><strong><?php echo l($node->title,"node/$node->nid") ?></strong>: 
				<?php print truncate_text($node->field_habitat[0]['value'],70) ?></p>

			<?php endforeach;?>
  </div>
</div>