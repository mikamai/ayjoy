
<div class="content_you_learn">

<h2 class="class-head"> <?php print t('YOU LEARN') ?> </h2>

<p class="p-claim"> <?php print t('Per tenersi aggiornati sulle piante che aiutano a stare in buona salute') ?> </p>

<?php
	print theme_views_view_list($view, $nodes, $type);
?>


<?php print theme('pager') ?>

</div>
