<?
  $info = array();

  if ($item['type']) {

    $info[] = check_plain($item['type']);

  }

  if ($item['user']) {

    $info[] = $item['user'];

  }

  if ($item['date']) {

    $info[] = format_date($item['date'], 'small');

  }

  if (is_array($item['extra'])) {

    $info = array_merge($info, $item['extra']);

  }

  $node = $item['node'];
?>

<?php //print_r($item); ?>
<tr>
    <td class="image_type"> <?php print theme('image_cat',strtolower($item['node']->type)) ?> </td>
       
    <td class="titolo">
		<p class="name"> 
			<?php $link = '<a href="'. check_url($item['link']) .'">'. check_plain($item['title']) .'</a>'; print $link; ?> 
		</p>
		
		<p class="category">
		<?php 
    		$taxonomy = array_keys($item['node']->taxonomy);
    		if (!empty($taxonomy)) {
    			print l($item['node']->taxonomy[$taxonomy[0]]->name,taxonomy_term_path($item['node']->taxonomy[$taxonomy[0]]));
    		}
    	?>
		</p>
		<p class="teaser"> <?php  print filter_xss(truncate_html($item['node']->teaser, 250), array('strong')); ?> </p>
    </td>
</tr>
