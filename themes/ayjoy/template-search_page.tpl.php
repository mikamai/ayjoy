<table class="search_table">
<thead> 
 <?php $arg = preg_replace('/type:.*/','',arg(2)) ?>
    <tr>
    	<th class="parola_cercata" colspan="2">  <span class="termine">"<?php print trim(implode(',',explode(',',$arg))) ?>" </span> <?php print  t('risultati ricerca') ?> <span class="risultati"><?php print count($results) ?>  </span> </th>
    </tr>
</thead>

<tbody>

<?php
  foreach ($results as $entry) {
    print theme('search_item', $entry, $type);
  }
 ?>

</tbody>
</table>

<p class"pager">
	<?php print theme('pager', NULL, 10, 0); ?>
</p>
