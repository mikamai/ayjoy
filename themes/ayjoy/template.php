<?php


// $Id: template.php,v 1.4.2.1 2007/04/18 03:38:59 drumm Exp $

/**
 * Sets the body-tag class attribute.
 *
 * Adds 'sidebar-left', 'sidebar-right' or 'sidebars' classes as needed.
 */

/* Helper include */
include_once('helper/TextHelper.php');
include_once('helper/DateHelper.php');
include_once('helper/UrlHelper.php');
include_once('helper/DBHelper.php');
include_once('helper/FormHelper.php');
include_once('helper/StoreHelper.php');
include_once('helper/ProductHelper.php');


drupal_add_css(path_to_theme().'/css/reset.css');
drupal_add_css(path_to_theme().'/css/layout.css');
drupal_add_css(path_to_theme().'/css/restyle.css');
drupal_add_css(path_to_theme().'/css/safari.css');

drupal_add_js('misc/collapse.js');
drupal_add_js(path_to_theme().'/js/jquery.selectbox-0.5.js');
drupal_add_js(path_to_theme().'/js/search_box.js');

function phptemplate_body_class($sidebar_left, $sidebar_right) {
  if ($sidebar_left != '' && $sidebar_right != '') {
    $class = 'sidebars';
  }
  else {
    if ($sidebar_left != '') {
      $class = 'sidebar-left';
    }
    if ($sidebar_right != '') {
      $class = 'sidebar-right';
    }
  }

  if (isset($class)) {
    print ' class="'. $class .'"';
  }
}


function ayjoy_regions() {
  return array(
       'left_content' => t('left_content'),
       'right_content' => t('right_content'),
	   /*'leftfashion' => t('leftfashion'),
	   'rightfashion' => t('rightfashion'),*/
       'content' => t('content'),
	   'loginbox' => t('loginbox'),
	    'logout' => t('logout'),
  	   'footer_area' => t('footer_area'),
		'language_drop' => t('language_drop')
  );
}
/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
/*
function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode(' › ', $breadcrumb) .'</div>';
  }
}
*/
function phptemplate_breadcrumb($breadcrumb) {
  $breadcrumb[] = t(drupal_get_title());
  return '<div class="breadcrumb"><div id="breadcrumb-inside">&raquo; '. implode($breadcrumb, ' &raquo; ') .'</div></div>';
}

/**
 * Allow themable wrapping of all comments.
 */
function phptemplate_comment_wrapper($content, $type = null) {
  if (isset($type)) $node_type = $type;

  if (!$content || $node_type == 'forum') {
    return '<div id="comments">'. $content . '</div>';
  }
  else {
    return '<div id="comments"><h2 class="comments">'. t('Comments') .'</h2>'. $content .'</div>';
  }
}

/**
 * Override or insert PHPTemplate variables into the templates.
 */
function _phptemplate_variables($hook, $vars) {



  if ($hook == 'page') {
    if ($secondary = menu_secondary_local_tasks()) {
      $output = '<span class="clear"></span>';
      $output .= "<ul class=\"tabs secondary\">\n". $secondary ."</ul>\n";
      $vars['tabs2'] = $output;
    }

    // Hook into color.module
    if (module_exists('color')) {
      _color_page_alter($vars);
    }

    if (module_exists('path')) {

	        $alias = drupal_get_path_alias(str_replace('/edit','',$_GET['q']));
	        if ($alias != $_GET['q']) {
	          $suggestions = array();

	          //var_dump($vars['node']); die;



	          $template_filename = 'page';
	          foreach (explode('/', $alias) as $path_part) {
	            $template_filename = $template_filename . '-' . $path_part;
	            $suggestions[] = $template_filename;
	          }

			 $template_filename = 'page';
			 /* First try to avoid locale */
	          if (module_exists('locale')) {
	          	$locale = locale_supported_languages();
	          	$langs = array_keys($locale['name']);
	          	foreach (explode('/', $alias) as $path_part) {
	          		if (!in_array($path_part,$langs)) {
	            		$template_filename = $template_filename . '-' . $path_part;
	            		$suggestions[] = $template_filename;
	          		}
	          	}
	          }
	        }
		}
	    $vars['template_files'] = $suggestions;

		/* Override head_title */
		if (drupal_get_title()) {
		    //$head_title = array(variable_get('site_name', 'Drupal'), strip_tags(drupal_get_title()));
			//$head_title = implode(' | ', $head_title);
			//$vars['head_title'] = $head_title;
			$vars['head_title'] = strip_tags(drupal_get_title());
		}


  }

  if ($hook == 'node') {
  	    $alias = drupal_get_path_alias(str_replace('/edit','',$_GET['q']));
	        if ($alias != $_GET['q']) {
	          $suggestions = array();
	          $template_filename = 'node';
	          $suggestions[] = 'node-'.$vars['node']->type;

	          /* First try to avoid locale */
	          if (module_exists('locale')) {
	          	$locale = locale_supported_languages();
	          	$langs = array_keys($locale['name']);
	          	foreach (explode('/', $alias) as $path_part) {
	          		if (!in_array($path_part,$langs)) {
	            		$template_filename = $template_filename . '-' . $path_part;
	            		$suggestions[] = $template_filename;
	          		}
	          	}
	          }

	          $template_filename = 'node';
	          foreach (explode('/', $alias) as $path_part) {
	            $template_filename = $template_filename . '-' . $path_part;
	            $suggestions[] = $template_filename;
	          }
	        }

	    $vars['template_files'] = $suggestions;
  }

  if (module_exists('advanced_forum')) {
    $vars = advanced_forum_addvars($hook, $vars);
  }
 //var_dump($vars['template_files']);
  return $vars;
}

/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs.
 *
 * @ingroup themeable
 */
function phptemplate_menu_local_tasks() {
  $output = '';

  if ($primary = menu_primary_local_tasks()) {
    $output .= "<ul class=\"tabs primary\">\n". $primary ."</ul>\n";
  }

  return $output;
}
/* Blocks */

function phptemplate_views_view_block_homepage_problems($view,$type,$nodes)
{
    return _phptemplate_callback('template-block-homepage_problems', array('nodes' => $nodes, 'view' => $view));
}

function phptemplate_views_view_piante_homepage($view,$type,$nodes)
{
    return _phptemplate_callback('template-block-piante_homepage', array('nodes' => $nodes, 'view' => $view));    
}

function phptemplate_views_view_block_homepage_edutainment($view,$type,$nodes)
{
    return _phptemplate_callback('template-block-homepage_edutainment', array('nodes' => $nodes, 'view' => $view));
}

function phptemplate_views_view_block_sidebar_edutainment($view,$type,$nodes)
{
    return _phptemplate_callback('template-block-sidebar_edutainment', array('nodes' => $nodes, 'view' => $view));
}

function phptemplate_views_view_block_sidebar_piante($view,$type,$nodes)
{
    return _phptemplate_callback('template-block-sidebar_piante', array('nodes' => $nodes, 'view' => $view));
}





function ayjoy_uc_marketing_special_products() {
  $product_types = module_invoke_all('product_types');
  $gridwidth = variable_get('uc_catalog_grid_display_width', 3);
  if (variable_get('uc_catalog_grid_display', false)) {
    $gridlines = variable_get('uc_marketing_special_block_lines', 1);
    $gridwidth = $gridwidth * $gridlines;
  }

  $gridwidth = 2;
  $products = array();

  /* OLD SQL
   * $sql = "SELECT DISTINCT(n.nid), n.title, p.sell_price
    FROM {node} n
      INNER JOIN {uc_products} AS p ON n.nid = p.nid
    WHERE n.status = 1
      AND n.sticky = 1
      AND n.type IN ('". implode("','", $product_types) ."')
    ORDER BY p.sell_price ASC";
   */

   $sql = "SELECT DISTINCT(n.nid), n.title, p.sell_price
    FROM {node} n
      INNER JOIN {uc_products} AS p ON n.nid = p.nid
      LEFT JOIN {i18n_node} i18n ON n.nid = i18n.nid
    WHERE n.status = 1
      AND n.sticky = 1
      AND i18n.language ='it'
      AND n.type IN ('". implode("','", $product_types) ."')
    ORDER BY n.changed DESC";

  //$sql_rewrite = i18n_db_rewrite_sql($sql, 'n', '');
  $sql_rewrite = array(
  'join' => "LEFT JOIN {i18n_node} i18n ON n.nid = i18n.nid",
  'where' => "i18n.language ='it' OR i18n.language ='' OR i18n.language IS NULL"
  );

 // $result = db_query_range(db_rewrite_sql($sql,'n', 'nid', $sql_rewrite), 0, $gridwidth);
  $result = db_query_range($sql, 0, $gridwidth);
  while ($node = db_fetch_object($result)) {
 	$node = node_load($node->nid);
 	$alias = drupal_get_path_alias($_GET['q']);
  	$lang = i18n_get_lang_prefix($alias);

  	/* se ci troviamo nella lingua originale */
	if (!in_array($lang, array_keys($node->translation))) {
  	  		$products[] = $node->nid;
  	}
  	else {
  		/* If exist node translated */
  		if (isset($node->translation[$lang])) {
  			$products[] = $node->translation[$lang]->nid;
  		}
  	}
  }

  if (count($products)) {
    $output .= '<h2 class="title special">' . variable_get('uc_marketing_special_block_title', t('Special Offers')) . '</h2>';
    $output .= theme('uc_catalog_products', $products);
    if (variable_get('uc_marketing_special_block_help_allowed', true)) {
      $output .= '<p class="special description">' . variable_get('uc_marketing_special_block_help', t("We're always looking for new ways to bring our products to the broadest audience. Here are our special offers for this month.")) . '</p>';
    }
  }


  return _phptemplate_callback('template_block_offerte_speciali',array('products' => $products));

}

function phptemplate_views_view_special_offers($view, $type, $nodes) {
	foreach ($nodes as $node) {
		$node = node_load($node->nid);
	 	$alias = drupal_get_path_alias($_GET['q']);
	  	$lang = i18n_get_lang_prefix($alias);

	  	/* se ci troviamo nella lingua originale */
	  	if (!in_array($lang, array_keys($node->translation))) {
	  		$products[] = $node->nid;
	  	}
	  	else {
	  		/* If exist node translated */
	  		if (isset($node->translation[$lang])) {
	  			$products[] = $node->translation[$lang]->nid;
	  		}
	  	}
	}
	return _phptemplate_callback('template_block_offerte_speciali', array('products' => $products));
}


function ayjoy_uc_product_add_to_cart_blocco_homepage($node) {

 if ($node->nid) {
    $form = drupal_get_form_raw('tw_cart_block_form_'. $node->nid, $node);
  }
  else {
    $form = drupal_get_form_raw('tw_cart_block_form', $node);
  }
  //var_dump($form);

  unset($form['qty']['#title']);
  $form['qty']['#value'] = 1;
  $form['qty']['#default_value'] = 1;
  $form['qty']['#size'] = 2;
  $form['qty']['#theme'] = 'my_textfield';

  $form['submit']['#value'] = t('Buy');
  //var_dump($form); die;
  //drupal_prepare_form('uc_product_add_to_cart_form_'. $node->nid, $form);
  return drupal_render_form('tw_cart_block_form_'. $node->nid, $form);
}


function ayjoy_uc_product_add_to_cart_blocco_catalogo($node) {

 if ($node->nid) {
    $form = drupal_get_form_raw('tw_cart_block_form_'. $node->nid, $node);
  }
  else {
    $form = drupal_get_form_raw('tw_cart_block_form', $node);
  }
  //var_dump($form);

  $form['qty']['#title'] = t('Item');
  $form['qty']['#value'] = 1;
  $form['qty']['#default_value'] = 1;
  $form['qty']['#size'] = 2;
  $form['qty']['#theme'] = 'my_textfield';

  $form['submit']['#value'] = t('Buy');
  //var_dump($form); die;
  //drupal_prepare_form('uc_product_add_to_cart_form_'. $node->nid, $form);
  return drupal_render_form('tw_cart_block_form_'. $node->nid, $form);
}


function ayjoy_my_textfield($element) {
 $size = $element['#size'] ? ' size="' . $element['#size'] . '"' : '';
  $class = array('form-text');
  $extra = '';
  $output = '';

  if ($element['#autocomplete_path']) {
    drupal_add_js('misc/autocomplete.js');
    $class[] = 'form-autocomplete';
    $extra =  '<input class="autocomplete" type="hidden" id="'. $element['#id'] .'-autocomplete" value="'. check_url(url($element['#autocomplete_path'], NULL, NULL, TRUE)) .'" disabled="disabled" />';
  }
  _form_set_class($element, $class);

  if (isset($element['#field_prefix'])) {
    $output .= '<span class="field-prefix">'. $element['#field_prefix'] .'</span> ';
  }

  $output .= '<input type="text" maxlength="'. $element['#maxlength'] .'" name="'. $element['#name'] .'" id="'. $element['#id'] .'" '. $size .' value="'. check_plain($element['#value']) .'"'. drupal_attributes($element['#attributes']) .' />';

  if (isset($element['#field_suffix'])) {
    $output .= ' <span class="field-suffix">'. $element['#field_suffix'] .'</span>';
  }

  return theme('my_form_element', $element, $output). $extra;
}

function ayjoy_my_form_element($element, $value) {

  $required = !empty($element['#required']) ? '<span class="form-required" title="'. t('This field is required.') .'">*</span>' : '';

  if (!empty($element['#title'])) {
    $title = $element['#title'];
    if (!empty($element['#id'])) {
      $output .= ' <label for="'. $element['#id'] .'">'. t('!title: !required', array('!title' => filter_xss_admin($title), '!required' => $required)) ."</label>\n";
    }
    else {
      $output .= ' <label>'. t('!title: !required', array('!title' => filter_xss_admin($title), '!required' => $required)) ."</label>\n";
    }
  }

  $output .= " $value\n";

  if (!empty($element['#description'])) {
    $output .= ' <div class="description">'. $element['#description'] ."</div>\n";
  }
  return $output;
}




function ayjoy_flat_links($links) {
  $output = '';

  if (count($links) > 0) {
    $output = '<ul>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = $key;

      // Automatically add a class to each link and also to each LI
      if (isset($link['attributes']) && isset($link['attributes']['class'])) {
        $link['attributes']['class'] .= ' ' . $key;
      }
      else {
        $link['attributes']['class'] = $key;
      }

      if ($i == $num_links) {
        $extra_class = 'last ';
  	    $output .= '<li '. drupal_attributes(array('class' => $extra_class)) .'>';
      }
      else {
        $output .= '<li>';
      }

      // Is the title HTML?
      $html = isset($link['html']) && $link['html'];

      // Initialize fragment and query variables.
      $link['query'] = isset($link['query']) ? $link['query'] : NULL;
      $link['fragment'] = isset($link['fragment']) ? $link['fragment'] : NULL;

      if (isset($link['href'])) {
        $output .= l($link['title'], $link['href'], $link['attributes'], $link['query'], $link['fragment'], FALSE, $html);
      }
      else if ($link['title']) {
        //Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (!$html) {
          $link['title'] = check_plain($link['title']);
        }
        $output .= '<span'. drupal_attributes($link['attributes']) .'>'. $link['title'] .'</span>';
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}


/* Menu links customized to managing (last/first attributes) */
function ayjoy_menu_links($links) {
  if (!count($links)) {
    return '';
  }

  $num_links = count($links);
  $i = 1;
  $extra_class = '';

  $level_tmp = explode('-', key($links));
  $level = $level_tmp[0];
  $output = "<ul class=\"links-$level\">\n";


  foreach ($links as $index => $link) {

  	if ($i == $num_links) {
        $extra_class = 'last';
  	}

    $output .= '<li';
    $attributes = '';
    if (stristr($index, 'active')) {
      $attributes .= 'active';
    }
    if ($i == $num_links) {
    	$attributes .= ' last';
    }

    $output .= ($attributes ? drupal_attributes(array('class' => $attributes)) : '').">". l($link['title'], $link['href'], $link['attributes'], $link['query'], $link['fragment']) ."</li>\n";
    $i++;
  }
 $output .= '</ul>';

  return $output;
}

function ayjoy_footer_links($delta) {
	/* use this for retrieve raw menu structure (with active trail) for custom menu */
	$links = menu_primary_links(1, $delta);
	print theme('menu_links',$links);
}



function ayjoy_uc_catalog_product_grid($products) {
  return _phptemplate_callback('template_lista_prodotti', array('products' => $products));
}

function ayjoy_search_box_header() {
	$form = drupal_retrieve_form('search_theme_form');
	drupal_prepare_form('search_theme_form',$form);

	$form['#theme'] = 'search_block_header';
	$form['search_theme_form_keys']['#type'] = 'my_textfield';

	$form['submit']['#type'] = 'button';
  	$form['submit']['#button_type'] = 'image';
  	$form['submit']['#attributes'] = array(
    	'src' =>  base_path(). path_to_theme() . '/css/images/search.png',
    	'alt' => t('Search')
  	);
	return drupal_render($form);
}

/* Funzione di theming specifica per il blocco di ricerca nell'header */
function ayjoy_search_block_header($form) {
	return drupal_render($form);
}


function ayjoy_button($element) {
  // following lines are copied directly from form.inc core file:

  //Make sure not to overwrite classes
  if (isset($element['#attributes']['class'])) {
    $element['#attributes']['class'] = 'form-'. $element['#button_type'] .' '. $element['#attributes']['class'];
  }
  else {
    $element['#attributes']['class'] = 'form-'. $element['#button_type'];
  }

  // here the novelty begins: check if #button_type is normal submit button or image button
  $return_string = '<input ';
  if ($element['#button_type'] == 'image') {
    $return_string .= 'type="image" ';
  }
  else {
    $return_string .= 'type="submit" ';
  }
//  $return_string .= (empty($element['#name']) ? '' : 'name="'. $element['#name'] .'" ') .'value="'. check_plain($element['#value']) .'" '. drupal_attributes($element['#attributes']) ." />\n";;
  return '<input type="'.$element['#button_type'].'" '. (empty($element['#name']) ? '' : 'name="'. $element['#name'] .'" ') .'id="'. $element['#id'] .'" value="'. check_plain($element['#value']) .'" '. drupal_attributes($element['#attributes']) ." />\n";
//  return $return_string;
}

function ayjoy_uc_catalog_browse($tid = 0) {
  drupal_add_css(drupal_get_path('module', 'uc_catalog') .'/uc_catalog.css');
  $cat = 0;
  $output = '';
  $catalog = uc_catalog_get_page((int)$tid);
  drupal_set_title(check_plain($catalog->name));
  drupal_set_breadcrumb(uc_catalog_set_breadcrumb($catalog->tid));
  $types = module_invoke_all('product_types');
  $links = array();
  $child_list = array();
  foreach ($catalog->children as $child) {
    if ($child->nodes) {
      $links[] = array('title' => $child->name . (variable_get('uc_catalog_breadcrumb_nodecount', false) ? ' ('. $child->nodes .')' : ''), 'href' => uc_catalog_path($child),
        'attributes' => array('rel' => 'tag'),
      );
    }
    if ($child->image) {
      $image = '<div id="'. $cat .'">';
      if (module_exists('imagecache')) {
        $image .= l(theme('imagecache', 'uc_category', $child->image['filepath']), uc_catalog_path($child), array(), null, null, false, true);
      }
      else {
        $image.= l(theme('image', $child->image['filepath']), uc_catalog_path($child), array(), null, null, false, true);
      }
      $image .= '</div>';
    }
    else {
      $image = '<div id="'. $cat .'"></div>';
    }
$cat++;
    $grandchildren = array();
    $j = 0;
    $max_gc_display = 3;
    foreach ($child->children as $i => $grandchild) {
      if ($j > $max_gc_display) {
        break;
      }
      $g_child_nodes = 0;
      foreach ($types as $type) {
        $g_child_nodes += taxonomy_term_count_nodes($grandchild->tid, $type);
      }
      if ($g_child_nodes) {
        $grandchildren[$i] = l($grandchild->name, uc_catalog_path($grandchild), array('class' => 'subcategory'));
        $j++;
      }
    }
    //$grandchildren = array_slice($grandchildren, 0, intval(count($grandchildren) / 2) + 1, true);
    if ($j > $max_gc_display) {
      array_push($grandchildren, l(t('More...'), uc_catalog_path($child), array('class' => 'subcategory')));
    }
    if ($child->nodes) {
      $cell_link = $image .'<strong>'. l($child->name, uc_catalog_path($child)) .'</strong>';
      if (variable_get('uc_catalog_show_subcategories', true)) {
        $cell_link .= "<br/><span>". implode(', ', $grandchildren) ."</span>\n";
      }
      $child_list[] = $cell_link;
    }
  }


  /* Twinbit fix */
  if ($catalog->image) {
    //$output .= theme('imagecache', 'uc_thumbnail', $catalog->image['filepath'], $catalog->name, $catalog->name, array('class' => 'category'));
  }
  $header = tapir_get_header('uc_product_table', array());
  $order = substr(tablesort_sql($header), 10);
  if (empty($_REQUEST['order'])) {
    $order = 'p.ordering, n.title, n.nid';
  }
  $product_types = module_invoke_all('product_types');

  $sql = "SELECT DISTINCT(n.nid), n.sticky, n.title, n.created, p.model, p.sell_price, p.ordering
    FROM {node} n
      INNER JOIN {term_node} tn ON n.nid = tn.nid
      INNER JOIN {uc_products} AS p ON n.vid = p.vid
    WHERE tn.tid = %d AND n.status = 1
      AND n.type IN ('". implode("','", $product_types) ."')
    ORDER BY ". $order;
  $sql_count = '';
  switch ($GLOBALS['db_type']) {
    case 'mysql':
    case 'mysqli':
      $sql_count = 'SELECT COUNT(DISTINCT(n.nid))
        FROM {node} n
          INNER JOIN {term_node} tn ON n.nid = tn.nid
          INNER JOIN {uc_products} AS p ON n.nid = p.nid
        WHERE tn.tid = %d
          AND n.status = 1
          AND n.type IN ("'. implode('","', $product_types) .'")';
      break;
    case 'pgsql':
      $sql_count = "SELECT DISTINCT n.nid, COUNT(*)
        FROM {node} n
          INNER JOIN {term_node} tn ON n.nid = tn.nid
          INNER JOIN {uc_products} AS p ON n.nid = p.nid
        WHERE tn.tid = %d
          AND n.status = 1
          AND n.type IN ('". implode("','", $product_types) ."')
        GROUP BY n.nid";
      break;
  }

  $sql = db_rewrite_sql($sql);
  $sql_count = db_rewrite_sql($sql_count);
  $catalog->products = array();
  $result = pager_query($sql, variable_get('uc_product_nodes_per_page', 12), 0, $sql_count, $catalog->tid);
  while ($node = db_fetch_object($result)) {
    $catalog->products[] = $node->nid;
  }
  if (count($catalog->products)) {
    if (count($links)) {
      $output .= theme('links', $links, array('class' => 'links inline')) ."<br />\n";
    }
    $output .= $catalog->description;
    $output .= theme('uc_catalog_products', $catalog->products);
    $output .= theme('pager');
  }
  else {
    // Display table of child categories similar to an osCommerce site's front page.
    $columns = variable_get('uc_catalog_category_columns', 3);
    $cat_rows = array();
    $row = array();
    $i = 1;
    foreach ($child_list as $cell) {
      $row[] = array('data' => $cell, 'class' => 'category');
      if ($i % $columns == 0) {
        $cat_rows[] = $row;
        $row = array();
      }
      $i++;
    }
    if (count($row) > 0 && count($row) < $columns) {
      if (count($cat_rows) >= 1) {
        $row = array_merge($row, array_fill(count($row), $columns - count($row), array('data' => '&nbsp;', 'class' => 'category')));
      }
      $cat_rows[] = $row;
    }
    $output .= $catalog->description;
   $output .= theme('table', array(), $cat_rows, array('class' => 'category'));
  }
  return _phptemplate_callback('template-page-you-buy', array('output' => $output));
 // return $output;
}


function ayjoy_uc_product_image($images) {
  static $rel_count = 0;
  $thickbox_enabled = module_exists('thickbox');
  $first = array_shift($images);
  //$output = '<div class="product_image">';
  if ($thickbox_enabled) {
    $output .= '<a href="'. check_url(file_create_url($first['filepath'])) .'" title="'. $first['title'] .'" class="thickbox" rel="field_image_cache_'. $rel_count .'">';
  }
  $output .= theme('imagecache', 'product', $first['filepath'], $first['alt'], $first['title']);
  //var_dump($output);
  /* if (count($images)) {
    $output .= '<br />'. t('Click for more images.');
  } */
  if ($thickbox_enabled) {
    $output .= '</a>';
  }
  $output .= '<br />';
  foreach ($images as $thumbnail) {
  	if ($thumbnail['filename']) {
	  	 if ($thickbox_enabled) {
	      $output .= '<a href="'. check_url(file_create_url($thumbnail['filepath'])) .'" title="'. $thumbnail['title'] .'" class="thickbox" rel="field_image_cache_'. $rel_count .'">';
	    }
	    $output .= theme('imagecache', 'uc_thumbnail', $thumbnail['filepath'], $thumbnail['alt'], $thumbnail['title']);
	    if ($thickbox_enabled) {
	      $output .= '</a>';
	    }
  	}

  }
//  $output .= '</div>';
  $rel_count++;
  return $output;
}

function ayjoy_views_view_ricerca_prodotti($view, $type, $nodes) {
	if ($type == 'block') {
		return _phptemplate_callback('template_block_ricerca_prodotti', array('view' => $view));
	}
	if ($type == 'page') {
		return _phptemplate_callback('template_page_ricerca_prodotti', array('view' => $view, 'nodes' => $nodes));
	}
}


function ayjoy_views_filters($form) {
	if ($form['#view_name'] == 'ricerca_prodotti') {

		$standard_options = array('**ALL**' => t('Select'));
		/* Delete views standard OPTIONS */
		unset($form['filter0']['#options']['**ALL**']);
		unset($form['filter0']['#options']['']);
		unset($form['filter1']['#options']['**ALL**']);
		unset($form['filter1']['#options']['']);
		unset($form['filter4']['#options']['**ALL**']);
		unset($form['filter4']['#options']['']);

		/* Order category */
		asort($form['filter0']['#options']);
		$form['filter0']['#options'] = array_merge($standard_options,$form['filter0']['#options']);
		$form['filter1']['#options'] = array_merge($standard_options,$form['filter1']['#options']);
		if ($form['filter4']['#options']) {
			$form['filter4']['#options'] = array_merge($standard_options,$form['filter4']['#options']);
		}

		/* Select titoli prodotti */
		$products = getAllProducts();
		asort($products);
		$form['filter2']['#type'] = 'select';
		$form['filter2']['#options'] = $standard_options + $products;
		unset($form['filter2']['#size']);

		/* Titles */
		$form['filter0']['#title'] = t('Area terapeautica');
		$form['filter1']['#title'] = t('Modalità');
		$form['filter2']['#title'] = t('Nome');
		$form['filter3']['#title'] = t('Keyword');
		$form['filter4']['#title'] = t('Pianta');

		/* Input size */
		//$form['filter2']['#size'] = 15;
		$form['filter3']['#size'] = 15;
		unset($form['filter3']);
		unset($form['submit']);
		return _phptemplate_callback('template_block_ricerca_prodotti',array('form_blocco' => $form));
	}
	/* Hardcoded call theme views filters for not managed exposed filters */
	else {
		return theme_views_filters($form);
	}
}



/* Blocco try also */
function ayjoy_views_view_block_prodotto_sidebar($view, $type, $nodes) {
	if ($type == 'block') {
		return _phptemplate_callback('template_block_sidebar_sinistra', array('node' => $nodes));
	}
}

function ay_search_page($results, $type) {
  $output = '<dl class="search-results">';

  foreach ($results as $entry) {
    $output .= theme('search_item', $entry, $type);
  }
  $output .= '</dl>';
  $output .= theme('pager', NULL, 10, 0);

  return $output;
}



/**
 * Theme the shopping cart block content.
 */
function ayjoy_uc_cart_block_content() {
  global $user;

  // Disabled until we figure out if this is actually screwing up caching. -RS
  //if (!$user->uid && variable_get('cache', 0) !== 0) {
  //  return t('<a href="!url">View</a> your shopping cart.', array('!url' => url('cart')));
  //}

  if (variable_get('uc_cart_show_help_text', FALSE)) {
    $output = '<span class="cart-help-text">'
            . variable_get('uc_cart_help_text', t('Click title to display cart contents.'))
             .'</span>';
  }

  $output .= '<div id="block-cart-contents">';

  $items = uc_cart_get_contents();

  $item_count = 0;
  if (!empty($items)) {
    $output .= '<table class="cart-block-table">'
              .'<tbody class="cart-block-tbody">';
    foreach ($items as $item) {
      $display_item = module_invoke($item->module, 'cart_display', $item);
      if (!empty($display_item)) {
        $output .= '<tr class="cart-block-item"><td class="cart-block-item-qty">'. $display_item['qty']['#default_value'] .'x</td>'
                  .'<td class="cart-block-item-title">'. $display_item['title']['#value'] .'</td>'
                  .'<td class="cart-block-item-price">'. uc_currency_format($display_item['#total']) .'</td></tr>';
        if ($display_item['options']['#value']) {
          $output .= '<tr><td colspan="3">'. $display_item['options']['#value'] .'</td></tr>';
        }
      }
      $total += ($item->price) * $item->qty;
      $item_count += $item->qty;
    }

    $output .= '</tbody></table>';
  }
  else {
    $output .= '<p>'. t('There are no products in your shopping cart.') .'</p>';
  }

  $output .= '</div>';

  $item_text = format_plural($item_count, '@count Item', '@count Items');
  $view = '('. l(t('View cart'), 'cart', array('rel' => 'nofollow')) .')';
  if (variable_get('uc_checkout_enabled', TRUE)) {
    $checkout = ' ('. l(t('Checkout'), 'cart/checkout', array('rel' => 'nofollow')) .')';
  }
  $output .= '<table class="cart-block-summary-table"><tbody class="cart-block-summary-tbody">'
            .'<tr class="cart-block-summary-tr"><td class="cart-block-summary-items">'
            . $item_text .'</td><td class="cart-block-summary-total">'
            .'<strong>'. t('Total:') .'</strong> '. uc_currency_format($total) .'</td></tr>';
  if ($item_count > 0) {
    $output .= '<tr><td colspan="2" class="cart-block-summary-checkout">'. $view . $checkout .'</td></tr>';
  }
  $output .= '</tbody></table>';

  return $output;
}


function ayjoy_views_view_page_problems_and_solutions($view, $type, $nodes) {
	return _phptemplate_callback('template-page-you-care', array('view' => $view, 'type' => $type, 'nodes' => $nodes));
}

function ayjoy_views_view_page_you_learn($view, $type, $nodes) {
	return _phptemplate_callback('template-page-you-learn', array('view' => $view, 'type' => $type, 'nodes' => $nodes));
}

function ayjoy_uc_cart_view_form($form) {
  drupal_add_css(drupal_get_path('module', 'uc_cart') .'/uc_cart.css');

  $output = '<div id="cart-form-products">'
          . tapir_get_table('uc_cart_view_table', $form) .'</div>';

  if (($page = variable_get('uc_continue_shopping_url', '')) != '<none>') {
    if (variable_get('uc_continue_shopping_type', 'link') == 'link') {
      $output .= '<div id="cart-form-buttons"><div id="continue-shopping-link">'
               . l(t('Continue shopping'), $page) .'</div>'
               . drupal_render($form) .'</div>';
    }
    else {
      $button = drupal_render($form['continue_shopping']);
      $output .= '<div id="cart-form-buttons"><div id="update-checkout-buttons">'
               . drupal_render($form) .'</div><div id="continue-shopping-button">'
               . $button .'</div></div>';
    }
  }
  else {
    $output .= '<div id="cart-form-buttons">'. drupal_render($form) .'</div>';
  }
  return $output;
}


function ayjoy_search_item($item, $type) {
  return _phptemplate_callback('template-search_item', array('item' => $item, 'type' => $type));
}

function ayjoy_search_page($results, $type) {
  return _phptemplate_callback('template-search_page', array('results' => $results, 'type' => $type));
}

function ayjoy_image_cat($cat)
{
    $cat = str_replace('_','-',$cat);
	$cat = str_replace(' ', '-', $cat);

	if ($cat == 'product')
    {
		$picture = path_to_theme()."/css/images/".'mini-buy.png';
    }

    elseif ($cat == 'problems-and-solutions')
    {
		$picture = path_to_theme()."/css/images/".'mini-care.png';
    }

    elseif ($cat == 'piante' || $cat == 'edutainment')
    {
		$picture = path_to_theme()."/css/images/".'mini-learn.png';
    }

    return theme('image', $picture, $alt, $alt, $options, false);
}


/*
function phptemplate_user_register_i18n($form) {
	$form['Informazioni cliente']['country'] = uc_country_select(uc_get_field_name('country'), $form['Informazioni cliente']['country']['#value'], NULL, 'name', uc_address_field_required('country'));
//	var_dump($form); die;
	return drupal_render($form);
}
*/