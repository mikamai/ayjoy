
<?php if($products) : ?>
<div id="offers-down">
  <div id="offers-up">
    <h2><?php print t('SPECIAL OFFERS');?></h2>


    <div id="line-1">
      <?php foreach ($products as $product): ?>
		<?php $i++; $images = array(); ?>
        	<?php $product = node_load($product); ?>

        <div class="offers-item-<?php print $i ?>">
		<?php	foreach ($product->field_image_cache as $image) : ?>
			<?php 	if ($image['filename']) $images[] = $image; ?>
		<?php endforeach; ?>

  		<?php
	  	if (module_exists('imagecache') && isset($product->field_image_cache) && file_exists($images[0]['filepath'])) {
  				print l(theme('imagecache', 'special_offer', $images[0]['filepath']), "node/".$product->nid, array(), null, null, false, true);
	  		}
		?>

		</div>
		<?php endforeach; ?>
		<br class="clear" />
   </div>

    <div id="line-2">
	<?php $i = 0 ;?>
	 <?php foreach ($products as $product): ?>
		<?php $product = node_load($product); ?>
		<?php $i++; ?>
		<div class="offers-item-<?php print $i ?>">
			<p class="claim">
		    	<?php print $product->field_claim[0]['value'] ?>
		    </p>
      	</div>
	 <?php endforeach; ?>
		<br class="clear" />
    </div>

    <div id="line-3">
	<?php $i = 0; ?>
			 <?php foreach ($products as $product): ?>
	        	<?php $product = node_load($product); ?>

				<?php $i++; ?>
			      <div class="offers-item-<?php print $i ?>">
					 <p class="total-price"> <?php print uc_currency_format($product->sell_price) ?></p>
					 <div class="offers-cart">
			              <div class="cart-image"><img title="cart" src="<?php print base_path() . path_to_theme().'/css/images/cart-2.png'; ?>"/></div>
					      <div class="cart-buy">
						     <?php
					         	print theme('uc_product_add_to_cart_blocco_homepage', $product);
					         ?>
							<br class="clear"/>
					        </div>
					      <br class="clear"/>
				  </div> </div>
			<?php endforeach; ?>
	<br class="clear" />
	</div>

	</div> </div>

<?php endif; ?>

