
<!-- FORM RETRIEVE -->

<?php
if (!isset($form_blocco)) {
  $form = drupal_retrieve_form('views_filters', $view);  
  $form['#action'] = url($view->url);
  drupal_process_form('views_filters', $form);
  print drupal_render_form('views_filters', $form);
}
?>

<!-- START TEMPLATE -->
<?php if (isset($form_blocco)) : ?>
	<div class="ricerca_field1">
		<?php $form_blocco['filter0']['#title'] = t('Per area terapeutica') ?>
		<?php print drupal_render($form_blocco['filter0']) ?>
	</div>
	<!-- 
		<div class="ricerca_field2">
			<?php print drupal_render($form_blocco['filter1']) ?>
		</div>
	-->
	<div class="ricerca_field3">
		<?php $form_blocco['filter0']['#title'] = t('Nome') ?>
		<?php print drupal_render($form_blocco['filter2']) ?>
	</div>
	<div class="ricerca_field4">
		<?php $form_blocco['filter0']['#title'] = t('Keyword') ?>
		<?php print drupal_render($form_blocco['filter3']) ?>
	</div>
	
	<?php print drupal_render($form_blocco) ?>
<?php endif;  ?>
