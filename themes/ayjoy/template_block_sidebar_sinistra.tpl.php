<?php $node = node_load($node[0]->nid);  ?>
<div class="blocco_try_also">
	
	<h2 class="arrow"> <?php print l(t('TRY ALSO'),'node/'.$node->nid) ?> </h2>	
	
	<p class="product_title">
		<?php print l(check_plain($node->title),'node/'.$node->nid) ?>
	</p>
	<p class="product_desc">
		<?php print filter_xss(truncate_html($node->body, 255, '...', FALSE, TRUE), array('strong', 'b')) ?>
	</p>
</div>
