<div id="products-list"> 
<?php foreach ($products as $product) : ?>
<?php    $product = node_load($product); $node = $product; ?>

  <div class="product-item">
    <div class="pruduct-item-image">
     
     <?php
     $images = array();
      $titlelink = l($product->title, "node/".$node->nid, array(), null, null, false, true); 
      foreach ($product->field_image_cache as $image) {
      	if ($image['filename']) $images[] = $image;
      }
      
      
	  if (module_exists('imagecache') && isset($product->field_image_cache) && file_exists($images[0]['filepath'])) {  	 
	  		print l(theme('imagecache', 'product_list', $images[0]['filepath']), "node/".$node->nid, array(), null, null, false, true);
	  }
     ?>
    </div>
<!-- 
    <div class="product-item-desc">
      <h3> <?php print $titlelink ?> </h3>
      <p>Prima descrizione del prodotto a destra dell'immagine</p>
    </div>
    -->
    
    <div class="product-item-desc-2">
          <h3> <?php print $titlelink ?> </h3>
      	  <p class="area-terapeutica"> <?php $taxonomy = array_keys($product->taxonomy); print $product->taxonomy[$taxonomy[0]]->name; ?> </p> 
          <p><?php print truncate_html($product->body, 255) ?> </p>	
          <p class="more"> <a aref="#"><?php print l(t('Read more'),'node/'.$product->nid) ?></a></p>
    </div>
    <br class="clear" />
    
    <div>
    <div id="product-item-cart-down">
      <div id="product-item-cart-up">
          <div class="cart-image"> <?php print theme('image',path_to_theme().'/css/images/cart-2.png') ?></div>      
          <p class="price"><span class="price"><?php print uc_currency_format($product->sell_price) ?></span></p>
          <div class="buy-link">
            <?php  
            	print theme('uc_product_add_to_cart_blocco_catalogo', $product); 
            ?>
            
          </div>
          <br class="clear" />
      </div>
    </div>
  </div>
  </div>
  <?php endforeach; ?>
</div>

