<!-- ELENCO PRODOTTI  -->


<?php
/* AUTO Redict to page product */
if ($_GET['filter2']) {
	//_pathauto_include();	
	//$node_title = pathauto_cleanstring(strtolower(check_plain($_GET['filter2'])));
	$node = node_load(check_plain($_GET['filter2']));
	if ($node->nid) {
		drupal_goto(url('node/'.check_plain($_GET['filter2']),NULL,NULL,TRUE));
	}

}
?>

<div id="products-list">

<?php /* <h1> <?php print t('SEARCH RESULTS') ?> </h1> */ ?>

<?php if (!count($nodes)) : ?>
	<p class="no-results">
		<?php print t('Nessun prodotto corrisponde ai criteri di ricerca') ?>
	</p>
	</div>
<?php else : ?>

<?php foreach ($nodes as $product) : ?>
<?php $product = node_load($product->nid) ?>
  <div class="product-item">
    <div class="pruduct-item-image">
    <?php
      $titlelink = l($product->title, "node/".$product->nid, array(), null, null, false, true);
	   
     $images = array();
    //  $titlelink = l($product->title, "node/".$node->nid, array(), null, null, false, true); 
      foreach ($product->field_image_cache as $image) {
      	if ($image['filename']) $images[] = $image;
      }
      
      
	  if (module_exists('imagecache') && isset($product->field_image_cache) && file_exists($images[0]['filepath'])) {  	 
	  		print l(theme('imagecache', 'product_list', $images[0]['filepath']), "node/".$product->nid, array(), null, null, false, true);
	  }
     
     ?>
    </div>
      <h3> <?php print $titlelink ?></h3>
      <p class="area-terapeutica"> <?php $taxonomy = array_keys($product->taxonomy); print $product->taxonomy[$taxonomy[0]]->name; ?> </p> 
      <p><?php print truncate_html($product->body, 255) ?> </p>
      <p class="more"> <?php print l(t('Read more'),'node/'.$product->nid) ?></p>
      <br class="clear">
    <div>
    <!-- Ora la barra con i dati carrello -->
    <div id="product-item-cart-down">
      <div id="product-item-cart-up">
          <div class="cart-image"> <?php print theme('image',path_to_theme().'/css/images/cart-2.png') ?></div>      
          <p class="price"><span class="price"><?php print uc_currency_format($product->sell_price) ?></span></p>
          <div class="buy-link">
            <?php  
            	print theme('uc_product_add_to_cart_blocco_catalogo', $product); 
            ?>
            
          </div>
          <br class="clear" />
      </div>
    </div>
  </div>
 </div>
<?php endforeach; ?>
</div>


<div class="pager">
  <?php print theme('pager') ?>
</div>

<?php endif; ?>
