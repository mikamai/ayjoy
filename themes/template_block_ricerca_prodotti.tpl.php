<?php

  $form = drupal_retrieve_form('views_filters', $view);
    
  $form['#action'] = url($view->url);
  drupal_process_form('views_filters', $form);

  print drupal_render_form('views_filters', $form);

 ?>
